<?php

class Common extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');

    }

    public function get_all($table)
    {
        $query = $this->db->get($table);  // Produces: SELECT * FROM mytable
        return $query->result();
    }

    public function get_one_item_where($field, $value, $table)
    {
        $this->db->where($field, $value);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_one_item_array($field, $value, $table)
    {
        $this->db->where($field, $value);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_two_item_where($field1, $value1, $field2, $value2, $table)
    {
        $this->db->where($field1, $value1);
        $this->db->where($field2, $value2);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_two_item_row($field1, $value1, $field2, $value2, $table)
    {
        $this->db->where($field1, $value1);
        $this->db->where($field2, $value2);
        $query = $this->db->get($table);
        return $query->row_array();

    }

    public function get_three_item_row($field1, $value1, $field2, $value2, $field3, $value3, $table)
    {
        $this->db->where($field1, $value1);
        $this->db->where($field2, $value2);
        $this->db->where($field3, $value3);
        $query = $this->db->get($table);
        return $query->row_array();
    }

    public function item_insert($table, $data)
    {
        $this->db->insert($table, $data);
        return $insert_id = $this->db->insert_id();
    }


    public function delete_item($field, $value, $table)
    {
        $this->db->where($field, $value);
        $this->db->delete($table);
    }

    public function update_item($field, $value, $table, $data)
    {
        $this->db->where($field, $value);
        $this->db->update($table, $data);
        return $this->db->affected_rows();
    }

    public function get_one_item_join_1($field, $value, $table_join, $where, $table)
    {
        $this->db->where($field, $value);
//        $this->db->join('comments', 'comments.id = blogs.id');
        $this->db->join($table_join, $where);

        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_one_item_join($field, $value, $table_join, $where, $table, $select = "*")
    {
        $this->db->select($select);
        $this->db->where($field, $value);
//        $this->db->join('comments', 'comments.id = blogs.id');
        $this->db->join($table_join, $where);

        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_two_item_join($field1, $value1, $field2, $value2, $table_join, $where, $table)
    {
        $this->db->where($field1, $value1);
        $this->db->where($field2, $value2);
//        $this->db->join('comments', 'comments.id = blogs.id');
        $this->db->join($table_join, $where);

        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_three_table_join_one_where($select, $table_join1, $where1, $table_join2, $where2, $table, $field1, $value1)
    {
        $this->db->select($select);
//        $this->db->from('table1');
        $this->db->from($table);
        $this->db->join($table_join1, $where1);
        $this->db->join($table_join2, $where2);
        $this->db->where($field1, $value1);

        $query = $this->db->get();
        return $query->result();

    }

    public function get_four_table_join_one_where($select, $table_join1, $where1, $table_join2, $where2, $table_join3, $where3, $table, $field1, $value1)
    {
        $this->db->select($select);
//        $this->db->from('table1');
        $this->db->from($table);
        $this->db->join($table_join1, $where1);
        $this->db->join($table_join2, $where2);
        $this->db->join($table_join3, $where3);
        $this->db->where($field1, $value1);

        $query = $this->db->get();
        return $query->result();

    }

    public function get_hash($value)
    {
        $query = $this->db->get("tbl_sec");
        $get_hash = $query->result();
        return md5(trim($value . $get_hash[0]->sec_cde));
    }
    //check session
    public function check_session(){
        $current_date_time=date("Y-m-d H:i:s");
        $this->db->where('login_time <=',$current_date_time );
        $this->db->where('logout_time >=', $current_date_time);
        $this->db->where('login_ip', $this->input->ip_address());
        $this->db->where('login_browser', $this->agent->browser());
        $this->db->where('sess_enc_val', $this->session->userdata("login_id"));
        $query= $this->db->get('admin_login_details');
        $result=$query->result();
        if(empty($result)){
           redirect('admins');
        }
    }
    public function check_session_1(){
        $current_date_time=date("Y-m-d H:i:s");
        $this->db->where('login_time <=',$current_date_time );
        $this->db->where('logout_time >=', $current_date_time);
        $this->db->where('login_ip', $this->input->ip_address());
        $this->db->where('login_browser', $this->agent->browser());
        $this->db->where('sess_enc_val', $this->session->userdata("login_id"));
        $query= $this->db->get('admin_login_details');
        $result=$query->result();
        if(empty($result)){
            return 0;
        }else{
            return 1;
        }
    }


}