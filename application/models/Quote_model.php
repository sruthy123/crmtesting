<?php

class Quote_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**************get total quote amount************/
    public function get_line_total($qty,$unit_of_work,$unit_price)
    {
        $unit_of_work=($unit_of_work==0)?1:$unit_of_work;
        return $qty*$unit_of_work*$unit_price;
    }

}