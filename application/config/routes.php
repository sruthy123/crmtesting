<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//ROUTE TO ADMIN MODULE
$route['admins'] = 'admin/admins';
$route['admins/(:any)'] = 'admin/admins/$1';
//ROUTE TO ADMIN SALES CONTROLLERS
$route['admins/sales/(:any)'] = 'admin/sales/$1';
$route['admins/sales/dashboard/(:any)'] = 'admin/sales/dashboard/$1';
$route['admins/sales/customers/(:any)'] = 'admin/sales/customers/$1';
$route['admins/sales/customers/(:any)/(:any)'] = 'admin/sales/customers/$1/$2';
$route['admins/sales/contacts/(:any)'] = 'admin/sales/contacts/$1';
$route['admins/sales/contacts/(:any)/(:any)'] = 'admin/sales/contacts/$1/$2';
$route['admins/sales/deals/(:any)'] = 'admin/sales/deals/$1';
$route['admins/sales/deals/(:any)/(:any)'] = 'admin/sales/deals/$1/$2';
$route['admins/sales/products_services/(:any)'] = 'admin/sales/products_services/$1';
$route['admins/sales/products_services/(:any)/(:any)'] = 'admin/sales/products_services/$1/$2';
$route['admins/sales/quotes/(:any)'] = 'admin/sales/quotes/$1';
$route['admins/sales/quotes/(:any)/(:any)'] = 'admin/sales/quotes/$1/$2';
//ROUTE TO ADMIN HRMS CONTROLLERS
$route['admins/hrms/(:any)'] = 'admin/hrms/$1';
$route['admins/hrms/people/(:any)'] = 'admin/hrms/people/$1';
$route['admins/hrms/people/(:any)/(:any)'] = 'admin/hrms/people/$1/$2';

//ROUTE TO ADMIN VENDOR CONTROLLER
$route['admins/([a-z]+)/(:any)'] = 'admin/vendors/$1';
$route['admins/vendors/vendors/(:any)'] = 'admin/vendors/vendors/$1';
$route['admins/vendors/vendors/(:any)/(:any)'] = 'admin/vendors/vendors/$1/$2';
//$route['admins/vendors/vendors/(:any)'] = 'admin/vendors/vendors/$1';

$controller_exceptions= array('admins');
$default_controller = "Client";
$route['default_controller'] =$default_controller;
$route["^((?!\b".implode('\b|\b',$controller_exceptions)."\b).*)$"] = $default_controller.'/$1';
$route['404_override'] = '';
