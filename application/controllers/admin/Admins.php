<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends CI_Controller
{

    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');

    }

    /**************load admin login************/
    public function index()
    {
        if (($this->common->check_session_1())) {
            redirect("admins/dashboard");
        } else {
            $this->load->view('admin/login');
        }
    }

    /**************check login************/
    public function check_login()
    {
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/login');
            } else {
                $username = trim($this->input->post('username'));
                $get_hash = $this->common->get_all("tbl_sec");
                $password = md5(trim($this->input->post('password') . $get_hash[0]->sec_cde));
                $get_user = $this->common->get_two_item_row("user_name", $username, "pass_word", $password, "admin_login");
                if (!empty($get_user)) {
                    $this->session->set_flashdata('success_msg', 'Login Success');
                    $data = array(
                        "login_time" => date("Y-m-d H:i:s"),
                        "logout_time" => date("Y-m-d H:i:s", time() + 7200),
                        "login_ip" => $this->input->ip_address(),
                        "login_browser" => $this->agent->browser()
                    );
                    $insert_id = $this->common->item_insert("admin_login_details", $data);
                    $current_date_time=date("Y-m-d H:i:s");
                    $datas = array("sess_enc_val" => $this->common->get_hash($insert_id .$current_date_time ));
                    $this->common->update_item("id", $insert_id, "admin_login_details", $datas);
                    $this->session->set_userdata('login_id', $this->common->get_hash($insert_id . $current_date_time));
                    redirect('admins/dashboard');
                } else {
                    $this->session->set_flashdata('error_msg', 'Invalid Login');
                    redirect('admins');
                }
            }

        }
    }

    /**************load admin dashboard************/
    public function dashboard()
    {
        $this->common->check_session();
        $data["active"] = "dashboard";
        $this->load->view("admin/dashboard", $data);
    }

    /**************logout************/
    public function logout()
    {
        $data = array("user_logout" => 1, "logout_time" => date("Y-m-d H:i:s"));
        $this->common->update_item("sess_enc_val", $this->session->userdata("login_id"), "admin_login_details", $data);
        $this->session->sess_destroy();
        redirect('admins');
    }
}
