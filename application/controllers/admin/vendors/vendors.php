<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vendors extends CI_Controller
{

    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');
        $this->common->check_session();
    }

    /**************load vendors list page************/
    public function vendors_list()
    {
        $data["active"] = "vendors";
        $data["vendors"] = $this->common->get_one_item_where("delete_status", 1, "vendors");
        $this->load->view('admin/vendors/vendors_list', $data);
    }

    /**************page to add vendor************/
    public function add_vendor()
    {
        $data["active"] = "vendors";
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('first_name', 'First name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('gender', 'gender', 'trim|required|numeric|callback_gender_check');
            $this->form_validation->set_rules('e_mail', 'E-Mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('address', 'address', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/vendors/add_vendor', $data);
            } else {
                $data = $this->input->post(array('first_name', 'last_name', 'gender', 'e_mail', 'phone', 'address'));
                $data["created_ip"] = $this->input->ip_address();
                $insert_id = $this->common->item_insert("vendors", $data);
                if ($insert_id) {
                    $data = array("encrypt_id" => $this->common->get_hash($insert_id . date("Y-m-d H:i:s")));
                    $this->common->update_item("id", $insert_id, "vendors", $data);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/vendors/vendors/vendors_list');
            }
        } else {
            $this->load->view('admin/vendors/add_vendor', $data);
        }
    }

    /**************view vendor details************/
    public function view_vendor_details($encrypt_id)
    {
        $data["active"] = "vendors";
        $data["vendor"] = $this->common->get_one_item_where("encrypt_id", $encrypt_id, "vendors");
        // var_dump($data["vendor"][0]->id);
        // exit;
        $this->load->view('admin/vendors/view_vendor_details', $data);
    }

    /**************edit form showing vendor details************/
    public function edit_vendor_details($encrypt_id)
    {
        $data["active"] = "vendors";
        $data["vendor"] = $this->common->get_one_item_where("encrypt_id", $encrypt_id, "vendors");
        $this->load->view('admin/vendors/edit_vendor_details', $data);
    }

    /**************edit vendor details************/
    public function editing_vendor_details()
    {
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('first_name', 'First name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('gender', 'gender', 'trim|required|numeric|callback_gender_check');
            $this->form_validation->set_rules('e_mail', 'E-Mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('address', 'address', 'trim|required');
            if ($this->form_validation->run() == false) {
                $data["active"] = "vendors";
                $data["vendor"] = $this->common->get_one_item_where("encrypt_id", $this->input->post('encrypt_id'), "vendors");
                $this->load->view('admin/vendors/edit_vendor_details', $data);
            } else {
                $data = $this->input->post(array('first_name', 'last_name', 'gender', 'e_mail', 'phone', 'address'));
                $affected_rows = $this->common->update_item("encrypt_id", $this->input->post('id'), "vendors", $data);
                if ($affected_rows) {
                    $data = array("updated_at" => date("Y-m-d H:i:s"));
                    $this->common->update_item("encrypt_id", $this->input->post('encrypt_id'), "vendors", $data);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/vendors/vendors/vendors_list');
            }
        }
    }

    /**************delete vendor details************/
    public function delete_vendor($encrypt_id)
    {
        $data = array("delete_status" => 0);
        $affected_rows = $this->common->update_item("encrypt_id", $encrypt_id, "vendors", $data);
        if ($affected_rows) {
            $this->session->set_flashdata('success_msg', 'Deleted successfully...!!!');
        } else {
            $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
        }
        redirect('admins/vendors/vendors/vendors_list');
    }

    /**************check correct value for gender************/
    public function gender_check($val)
    {
        if ($val == 1 || $val == 2) { //1=>male 2=>female no other values are possible
            return TRUE;
        } else {
            $this->form_validation->set_message('gender_check', 'The {field} field is not correct value');
            return FALSE;
        }
    }
}
