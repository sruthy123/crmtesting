<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller
{
    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');
        $this->common->check_session();

    }

    /**************view customer************/

    public function view_customers()
    {
        $data["active"] = "accounts";
        $data["accounts"] = $this->common->get_one_item_where("delete_status", 1, "account_personal_details");
        $this->load->view("admin/sales/view_customers", $data);
    }

    /************** new account add form************/
    public function add_customer()
    {
        $data["active"] = "accounts";
        $data["countries"] = $this->common->get_all("countries");
        $data["default_languages"] = $this->common->get_all("default_language");
        $data["currencies"] = $this->common->get_all("currencies");
        $this->load->view("admin/sales/add_customer", $data);
    }

    /************** new account add form************/
    public function adding_customer()
    {
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('customer_type', 'customer type', 'trim|required');
            $this->form_validation->set_rules('company', 'Company', 'trim|required|max_length[30]');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('website', 'website', 'trim|required|callback_check_valid_url');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('e_mail', 'E-Mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('currency', 'currency', 'trim|required');
            $this->form_validation->set_rules('default_language', 'default language', 'trim|required');
            $this->form_validation->set_rules('address', 'address', 'trim|required');
            $this->form_validation->set_rules('city', 'city', 'trim|required');
            $this->form_validation->set_rules('state', 'state', 'trim|required');
            $this->form_validation->set_rules('zip_code', 'zip code', 'trim|required');
            $this->form_validation->set_rules('country_id', 'country', 'trim|required');
            $this->form_validation->set_rules('billing_street', 'street', 'trim|required');
            $this->form_validation->set_rules('billing_city', 'city', 'trim|required');
            $this->form_validation->set_rules('billing_state', 'state', 'trim|required');
            $this->form_validation->set_rules('billing_zip_code', 'zip code', 'trim|required');
            $this->form_validation->set_rules('billing_country', 'country', 'trim|required');
            if ($this->form_validation->run() == false) {
                $data["active"] = "accounts";
                $data["countries"] = $this->common->get_all("countries");
                $data["default_languages"] = $this->common->get_all("default_language");
                $data["currencies"] = $this->common->get_all("currencies");
                $this->load->view("admin/sales/add_customer", $data);
            } else {
                $data = $this->input->post(array('customer_type', 'company', 'parent_account',
                    'phone', 'e_mail', 'website', 'currency', 'default_language',
                    'address', 'city', 'state', 'zip_code', 'country_id'));
                $data["created_ip"] = $this->input->ip_address();
                $insert_id = $this->common->item_insert("account_personal_details", $data);
                if ($insert_id) {
                    $encrypt_id = $this->common->get_hash($insert_id . date("Y-m-d H:i:s"));
                    $data = array("encrypt_id" => $encrypt_id);
                    $affected_rows = $this->common->update_item("id", $insert_id, "account_personal_details", $data);
                    if ($affected_rows) {
                        $billing_data = $this->input->post(array('billing_street', 'billing_city', 'billing_state', 'billing_zip_code', 'billing_country'));
                        $billing_data["account_id"] = $encrypt_id;
                        $this->common->item_insert("account_billing_address", $billing_data);
                        $shipping_data = $this->input->post(array('shipping_street', 'shipping_city', 'shipping_state', 'shipping_zip_code', 'shipping_country'));
                        $shipping_data["account_id"] = $encrypt_id;
                        $this->common->item_insert("account_shipping_address", $shipping_data);
                    }
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/sales/customers/view_customers');
            }
        }
    }

    /**************view account details************/
    public function view_account_details($encrypt_id)
    {
        $data["active"] = "accounts";
        $data["personal_details"] = $this->common->get_four_table_join_one_where("account_personal_details.*,countries.long_name,currencies.name,default_language.language", "countries", "countries.country_id=account_personal_details.country_id", "currencies", "currencies.id=account_personal_details.currency", "default_language", "default_language.id=account_personal_details.default_language", "account_personal_details", "account_personal_details.encrypt_id", $encrypt_id);
        $data["billing_address"] = $this->common->get_one_item_join("account_id", $encrypt_id, "countries", "countries.country_id=account_billing_address.billing_country", "account_billing_address");
        $data["shipping_address"] = $this->common->get_one_item_join("account_id", $encrypt_id, "countries", "countries.country_id=account_shipping_address.shipping_country", "account_shipping_address");
        $this->load->view("admin/sales/view_account_details", $data);
    }

    /**************edit account details************/
    public function edit_account_details()
    {
        $data["active"] = "accounts";

        $this->load->view("admin/sales/edit_account_details", $data);
    }

    /**************valid_url check************/
    public function check_valid_url($param)
    {
        if (!filter_var($param, FILTER_VALIDATE_URL)) {
            $this->form_validation->set_message('check_valid_url', 'The {field} must be a valid url');
            return FALSE;
        } else {
            return TRUE;
        }

    }

    /**************delete customer details************/
    public function delete_customer($encrypt_id)
    {
        $data = array("delete_status" => 0);
        $affected_rows = $this->common->update_item("encrypt_id", $encrypt_id, "account_personal_details", $data);
        if ($affected_rows) {
            $this->session->set_flashdata('success_msg', 'Deleted successfully...!!!');
        } else {
            $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
        }
        redirect('admins/sales/customers/view_customers');
    }
}
