<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends CI_Controller
{

    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');
        $this->common->check_session();

    }

    /**************load all contacts************/
    public function view_all_contacts()
    {
        $data["active"] = "contacts";
//        $data["contacts"] = $this->common->get_one_item_where("delete_status", 1, "contact");
        $data["contacts"] = $this->common->get_one_item_join("contact.delete_status", 1, "account_personal_details", "account_personal_details.encrypt_id=contact.company_id", "contact","contact.*");
        $this->load->view("admin/sales/view_all_contacts", $data);
    }

    /**************view contact details of individual customer************/
    public function view_contact($encrypt_id)
    {
        $data["active"] = "contacts";
        $data["encrypt_id"] = $encrypt_id;
        $data["contacts"] = $this->common->get_two_item_where("delete_status", 1, "company_id", $encrypt_id, "contact");
        $this->load->view("admin/sales/view_contact", $data);
    }

    /**************Add new contact details of an account************/
    public function add_new_contact($encrypt_id)
    {
        $data["active"] = "contacts";
        $data["company_details"] = $this->common->get_one_item_where("encrypt_id", $encrypt_id, "account_personal_details");
        $this->load->view("admin/sales/add_new_contact", $data);
    }

    /**************Adding new contact under an account************/
    public function adding_new_contact()
    {
        if (isset($_POST["submit"])) {
            $encrypt_id = $this->input->post('company_id');
            $this->form_validation->set_rules('first_name', 'First name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('tittle', 'tittle', 'trim|required');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('e_mail', 'E-Mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('website', 'website', 'trim|required|callback_check_valid_url');
            $this->form_validation->set_rules('department', 'department', 'trim|required');
            if ($this->form_validation->run() == false) {
                $data["active"] = "contacts";
                $data["company_details"] = $this->common->get_one_item_where("encrypt_id", $encrypt_id, "account_personal_details");
                $this->load->view("admin/sales/add_new_contact", $data);
            } else {
                $data = $this->input->post(array('prefix', 'first_name', 'last_name', 'tittle', 'phone', 'e_mail', 'website', 'department', 'company_id'));
                $data["created_ip"] = $this->input->ip_address();
                $insert_id = $this->common->item_insert("contact", $data);
                if ($insert_id) {
                    $data = array("encrypt_id" => $this->common->get_hash($insert_id));
                    $this->common->update_item("id", $insert_id, "contact", $data);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/sales/contacts/view_all_contacts');
            }

        }
    }

    /**************Load edit form for contact************/
    public function edit_contact($encrypt_id)
    {
        $data["active"] = "contacts";
        $data["contact"] = $this->common->get_one_item_join("contact.encrypt_id", $encrypt_id, "account_personal_details", "contact.company_id=account_personal_details.encrypt_id", "contact","contact.*,account_personal_details.company");
        $this->load->view("admin/sales/edit_contact", $data);
    }
    /**************editing contact************/
    public function editing_contact(){
        if (isset($_POST["submit"])) {
             $encrypt_id = $this->input->post('encrypt_id');
            $this->form_validation->set_rules('first_name', 'First name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('tittle', 'tittle', 'trim|required');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('e_mail', 'E-Mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('website', 'website', 'trim|required|callback_check_valid_url');
            $this->form_validation->set_rules('department', 'department', 'trim|required');
            if ($this->form_validation->run() == false) {
                $data["active"] = "contacts";
                $data["contact"] = $this->common->get_one_item_join("contact.encrypt_id", $encrypt_id, "account_personal_details", "contact.company_id=account_personal_details.encrypt_id", "contact","contact.*,account_personal_details.company");
                $this->load->view("admin/sales/edit_contact", $data);
            } else {
                $data = $this->input->post(array('prefix', 'first_name', 'last_name', 'tittle', 'phone', 'e_mail', 'website', 'department', 'company_id'));
                $affected_rows = $this->common->update_item("encrypt_id",$encrypt_id,"contact", $data);
                if ($affected_rows) {
                    $data["updated_ip"] = $this->input->ip_address();
                    $data["updated_at"] = date("y-m-d H:i:s");
                    $this->common->update_item("encrypt_id",$encrypt_id,"contact", $data);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/sales/contacts/view_all_contacts');
            }

        }
    }

    /**************valid_url check************/
    public function check_valid_url($param)
    {
        if (!filter_var($param, FILTER_VALIDATE_URL)) {
            $this->form_validation->set_message('check_valid_url', 'The {field} must be a valid url');
            return FALSE;
        } else {
            return TRUE;
        }

    }

    /**************view contact deails************/
    public function view_contact_details($encrypt_id)
    {
        $data["active"] = "contacts";
        $data["contact"] = $this->common->get_one_item_join("contact.encrypt_id", $encrypt_id, "account_personal_details", "contact.company_id=account_personal_details.encrypt_id", "contact","contact.*,account_personal_details.company");
//        $data["contact"] = $this->common->get_three_table_join_one_where("contact.*","account_personal_details","account_personal_details.encrypt_id=contact.company_id","contact","contact.encrypt_id",$encrypt_id);
        $this->load->view("admin/sales/view_contact_details", $data);
    }

    /**************delete contact details************/
    public function delete_contact($encrypt_id)
    {
        $data = array("delete_status" => 0);
        $affected_rows = $this->common->update_item("encrypt_id", $encrypt_id, "contact", $data);
        if ($affected_rows) {
            $this->session->set_flashdata('success_msg', 'Deleted successfully...!!!');
        } else {
            $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
        }
        redirect('admins/sales/contacts/view_all_contacts');
    }

}
