<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');
        $this->common->check_session();
    }

    /**************load dash board************/
    public function index()
    {
        $data["active"] = "sales_dashboard";
        $this->load->view('admin/sales/dashboard', $data);
    }

}
