<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deals extends CI_Controller
{
    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');
        $this->common->check_session();

    }

    /**************add new deal************/
    public function add_new_deal()
    {
        $data["active"] = "deals";
        $data["deal_stages"] = $this->common->get_all("deal_stages");
        $data["accounts"] = $this->common->get_one_item_where("delete_status", 1, "account_personal_details");
        $data["contacts"] = $this->common->get_one_item_where("delete_status", 1, "contact");
        $this->load->view('admin/sales/add_new_deal', $data);
    }

    public function adding_new_deal()
    {
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('deal_name', 'Deal name', 'trim|required|alpha|max_length[40]');
            $this->form_validation->set_rules('deal_stage', 'Deal stage', 'trim|required');
            $this->form_validation->set_rules('amount', 'amount', 'trim|required');
            $this->form_validation->set_rules('close_date', 'close date', 'trim|required|regex_match[/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/]');
            $this->form_validation->set_rules('company', 'company', 'trim|required');
            $this->form_validation->set_rules('probability', 'probability', 'trim|required');
            $this->form_validation->set_rules('expected_revenue', 'expected revenue', 'trim|required');
            if ($this->form_validation->run() == false) {
                $data["active"] = "deals";
                $data["deal_stages"] = $this->common->get_all("deal_stages");
                $data["accounts"] = $this->common->get_one_item_where("delete_status", 1, "account_personal_details");
                $data["contacts"] = $this->common->get_one_item_where("delete_status", 1, "contact");
                $this->load->view('admin/sales/add_new_deal', $data);
            } else {
                $data = $this->input->post(array('deal_name',
                    'deal_stage', 'amount', 'close_date', 'company', 'contact', 'next_action', 'probability', 'expected_revenue'));
                $data["created_ip"] = $this->input->ip_address();
                $insert_id = $this->common->item_insert("deals", $data);
                if ($insert_id) {
                    $data = array("encrypt_id" => $this->common->get_hash($insert_id));
                    $this->common->update_item("id", $insert_id, "deals", $data);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/sales/deals/view_deals');
            }
        }

    }

    /**************view all deals************/
    public function view_deals()
    {
        $data["active"] = "deals";
        //$data["deals"] = $this->common->get_one_item_where("delete_status", 1, "deals");
        $data["deals"] = $this->common->get_one_item_join("deals.delete_status", 1, "account_personal_details", "deals.company=account_personal_details.encrypt_id", "deals", "deals.*,account_personal_details.company as company_name");
        $this->load->view('admin/sales/view_deals', $data);
    }

    /**************view deal details************/
    public function view_deal_details($encrypt_id)
    {
        $data["active"] = "deals";
        $data["deal"] = $this->common->get_three_table_join_one_where("*", "account_personal_details", "account_personal_details.encrypt_id=deals.company", "deal_stages", "deal_stages.id=deals.deal_stage", "deals", "deals.encrypt_id", $encrypt_id);
        if (!empty($data["deal"][0]->contact)) {
            $data["contact"] = $this->common->get_one_item_where("encrypt_id", $data["deal"][0]->contact, "contact");
        } else {
            $data["contact"] = array();
        }
        $this->load->view('admin/sales/view_deal_details', $data);
    }

    /**************Load edit form for deal************/
    public function edit_deal($encrypt_id)
    {
        $data["active"] = "deals";
        $data["deal"] = $this->common->get_three_table_join_one_where("deals.*", "account_personal_details", "account_personal_details.encrypt_id=deals.company", "deal_stages", "deal_stages.id=deals.deal_stage", "deals", "deals.encrypt_id", $encrypt_id);
        $data["deal_stages"] = $this->common->get_all("deal_stages");
        $data["accounts"] = $this->common->get_one_item_where("delete_status", 1, "account_personal_details");
        $data["contacts"] = $this->common->get_one_item_where("delete_status", 1, "contact");
        $this->load->view("admin/sales/edit_deal", $data);
    }
    /**************editing deal************/
    public function editing_deal(){
        if (isset($_POST["submit"])) {
            $encrypt_id=$this->input->post("encrypt_id");
            $this->form_validation->set_rules('deal_name', 'Deal name', 'trim|required|alpha|max_length[40]');
            $this->form_validation->set_rules('deal_stage', 'Deal stage', 'trim|required');
            $this->form_validation->set_rules('amount', 'amount', 'trim|required');
            $this->form_validation->set_rules('close_date', 'close date', 'trim|required|regex_match[/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/]');
            $this->form_validation->set_rules('company', 'company', 'trim|required');
            $this->form_validation->set_rules('probability', 'probability', 'trim|required');
            $this->form_validation->set_rules('expected_revenue', 'expected revenue', 'trim|required');
            if ($this->form_validation->run() == false) {
                $data["active"] = "deals";
                $data["deal"] = $this->common->get_three_table_join_one_where("deals.*", "account_personal_details", "account_personal_details.encrypt_id=deals.company", "deal_stages", "deal_stages.id=deals.deal_stage", "deals", "deals.encrypt_id", $encrypt_id);
                $data["deal_stages"] = $this->common->get_all("deal_stages");
                $data["accounts"] = $this->common->get_one_item_where("delete_status", 1, "account_personal_details");
                $data["contacts"] = $this->common->get_one_item_where("delete_status", 1, "contact");
                $this->load->view("admin/sales/edit_deal", $data);
            } else {
                $data = $this->input->post(array('deal_name',
                    'deal_stage', 'amount', 'close_date', 'company', 'contact', 'next_action', 'probability', 'expected_revenue'));
                $affected_rows = $this->common->update_item("encrypt_id",$encrypt_id,"deals", $data);
                if ($affected_rows) {
                    $data["updated_ip"] = $this->input->ip_address();
                    $data["updated_at"] = date("y-m-d H:i:s");
                    $this->common->update_item("encrypt_id",$encrypt_id,"deals", $data);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/sales/deals/view_deals');
            }
        }

    }

    /**************delete deal details************/
    public function delete_deal($encrypt_id)
    {
        $data = array("delete_status" => 0);
        $affected_rows = $this->common->update_item("encrypt_id", $encrypt_id, "deals", $data);
        if ($affected_rows) {
            $this->session->set_flashdata('success_msg', 'Deleted successfully...!!!');
        } else {
            $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
        }

        redirect('admins/sales/deals/view_deals');
    }
}
