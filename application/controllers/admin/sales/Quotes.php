<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotes extends CI_Controller
{
    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');
        $this->load->model('quote_model');
        $this->common->check_session();

    }

    /**************list all quotes************/
    public function list_quotes()
    {
        $data["active"] = "quotes";
        $data["quotes"] = $this->common->get_one_item_join("quote_basic_details.delete_status", 1, "account_personal_details", "account_personal_details.encrypt_id=quote_basic_details.account_id", "quote_basic_details", "quote_basic_details.*,account_personal_details.company");
        foreach ($data["quotes"] as $key => $quote) {
            // $datas=$this->quote_model->get_quote_total($quote->encrypt_id);
            $quote_items = $this->common->get_one_item_where("quote_id", $quote->encrypt_id, "quote_items");
            $quote_total = 0;
            foreach ($quote_items as $quote_item) {
                $line_total = $this->quote_model->get_line_total($quote_item->qty, $quote_item->unit_of_work, $quote_item->unit_price);
                $quote_total = $quote_total + $line_total;
            }
            $data["quotes"][$key]->total = $quote_total;
            $data["quotes"][$key]->quote_no = ($data["quotes"][$key]->id) + 1000;
        }
        $this->load->view("admin/sales/list_quotes", $data);
    }

    /**************add quote************/
    public function add_new_quote()
    {
        $data["active"] = "quotes";
        //create quote only under closed won stage
        $data["deals"] = $this->common->get_two_item_where("delete_status", 1, "deal_stage", 6, "deals");
        $data["accounts"] = $this->common->get_one_item_where("delete_status", 1, "account_personal_details");
        $data["product_services"] = $this->common->get_one_item_where("delete_status", 1, "product_services");
        $data["terms_conditions"] = $this->common->get_all("terms_conditions");
        $data["countries"] = $this->common->get_all("countries");
        $this->load->view("admin/sales/add_new_quote", $data);
    }

    /**************adding quote************/
    public function adding_quote()
    {
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('name', 'name', 'trim|required');
            $this->form_validation->set_rules('account_id', 'account', 'trim|required');
            $this->form_validation->set_rules('deal_id', 'deal', 'trim|required');
            $this->form_validation->set_rules('item_id[]', 'items', 'trim|required');
            $this->form_validation->set_rules('product_description[]', 'product description', 'trim|required');
            $this->form_validation->set_rules('qty[]', 'quantity', 'trim|required|numeric');
            $this->form_validation->set_rules('unit_of_work[]', 'unit of work', 'trim|numeric');
            $this->form_validation->set_rules('unit_time[]', 'unit time', 'trim|required');
            $this->form_validation->set_rules('unit_price[]', 'unit price', 'trim|required|numeric');
            $this->form_validation->set_rules('terms_conditions', 'terms and conditions', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                redirect("admins/sales/quotes/add_new_quote");
            } else {
                $data = $this->input->post(array('name', 'account_id', 'deal_id'));
                $data["created_ip"] = $this->input->ip_address();
                $insert_id = $this->common->item_insert("quote_basic_details", $data);
                if ($insert_id) {
                    $encrypt_quote_id = $this->common->get_hash($insert_id);
                    $data = array("encrypt_id" => $encrypt_quote_id);
                    $affected_rows = $this->common->update_item("id", $insert_id, "quote_basic_details", $data);
                    if ($affected_rows) {
                        $product_details = $this->input->post(array('item_id', 'product_description', 'qty', 'unit_of_work', 'unit_time', 'unit_price'));
                        $total_items = count($product_details["item_id"]);
                        $product["quote_id"] = $encrypt_quote_id;
                        for ($i = 0; $i < $total_items; $i++) {
                            $product["item_id"] = $product_details["item_id"][$i];
                            $product["product_description"] = $product_details["product_description"][$i];
                            $product["qty"] = $product_details["qty"][$i];
                            $product["unit_of_work"] = $product_details["unit_of_work"][$i];
                            $product["unit_time"] = $product_details["unit_time"][$i];
                            $product["unit_price"] = $product_details["unit_price"][$i];
                            $insert_id = $this->common->item_insert("quote_items", $product);
                            $data = array("encrypt_id" => $this->common->get_hash($insert_id));
                            $this->common->update_item("id", $insert_id, "quote_items", $data);
                        }
                        $terms_conditions = $this->input->post(array('terms_conditions'));
                        $terms_conditions["quote_id"]=$encrypt_quote_id;
                        $insert_id = $this->common->item_insert("quote_terms_conditions", $terms_conditions);
                        $data = array("encrypt_id" => $this->common->get_hash($insert_id));
                        $this->common->update_item("id", $insert_id, "quote_terms_conditions", $data);
                    }
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');

                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/sales/quotes/list_quotes');
            }
        }
    }

    public function view_quote_details($encrypt_id)
    {
        $data["active"] = "quotes";
        $data["quote_basic_details"] = $this->common->get_three_table_join_one_where("quote_basic_details.name,quote_basic_details.encrypt_id,quote_basic_details.account_id,account_personal_details.company,deals.deal_name", "account_personal_details", "account_personal_details.encrypt_id=quote_basic_details.account_id", "deals", "deals.encrypt_id=quote_basic_details.deal_id", "quote_basic_details", "quote_basic_details.encrypt_id", $encrypt_id);
        $data["billing_address"] = $this->common->get_one_item_join("account_id", $data["quote_basic_details"][0]->account_id, "countries", "account_billing_address.billing_country=countries.country_id", "account_billing_address", "*");
        $data["shipping_address"] = $this->common->get_one_item_join("account_id", $data["quote_basic_details"][0]->account_id, "countries", "account_shipping_address.shipping_country=countries.country_id", "account_shipping_address", "*");
        $data["quote_items"] = $this->common->get_one_item_join("quote_id", $data["quote_basic_details"][0]->encrypt_id, "product_services", "quote_items.item_id=product_services.encrypt_id", "quote_items", "quote_items.*,product_services.name");
        $quote_items = $this->common->get_one_item_where("quote_id", $data["quote_items"][0]->encrypt_id, "quote_items");
        $quote_total = 0;
        foreach ($quote_items as $key=>$quote_item) {
            $line_total = $this->quote_model->get_line_total($quote_item->qty, $quote_item->unit_of_work, $quote_item->unit_price);
            $data["quote_items"][$key]->line_total=$line_total;
            $quote_total = $quote_total + $line_total;
        }
        $data["total_amount"] = $quote_total;
        $data["terms_conditions"]=$this->common->get_one_item_where("encrypt_id", $data["quote_basic_details"][0]->encrypt_id, "quote_terms_conditions");
        $this->load->view("admin/sales/view_quote_details", $data);
    }

    /**************delete quote details************/
    public function delete_quote_details($encrypt_id)
    {
        $data = array("delete_status" => 0);
        $affected_rows = $this->common->update_item("encrypt_id", $encrypt_id, "quote_basic_details", $data);
        if ($affected_rows) {
            $this->session->set_flashdata('success_msg', 'Deleted successfully...!!!');
        } else {
            $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
        }
        redirect('admins/sales/quotes/list_quotes');
    }

    /**************ajax ----for select box to fill shipping and billing address according to account select box************/
    public function get_account_details()
    {
        $account_id = $this->input->post("account_id");
        $data["shipping_address"] = $this->common->get_one_item_where("account_id", $account_id, "account_shipping_address");
        $data["billing_address"] = $this->common->get_one_item_where("account_id", $account_id, "account_billing_address");
        echo json_encode($data);

    }

}
