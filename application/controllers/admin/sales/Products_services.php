<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_services extends CI_Controller
{
    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');
        $this->common->check_session();

    }

    /**************load all contacts************/
    public function view_products_services()
    {
        $data["active"] = "products";
        $data["product_services"] = $this->common->get_one_item_where("delete_status", 1, "product_services");
        $this->load->view("admin/sales/view_products_services", $data);
    }

    /**************load all product list************/
    public function add_new_product()
    {
        $data["active"] = "products";
        $data["roles"] = $this->common->get_all("designations");
        $data["vendors"] = $this->common->get_one_item_where("delete_status", 1, "vendors");
        $this->load->view("admin/sales/add_new_product", $data);
    }

    /**************adding new product / services************/
    public function adding_new_product()
    {
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('name', 'name', 'trim|required|max_length[60]');
            $this->form_validation->set_rules('type', 'type', 'trim|required|callback_type_check');
            $this->form_validation->set_rules('unit_type', 'unit type', 'trim|required');
            $this->form_validation->set_rules('price', 'price', 'trim|required|regex_match[/^-?[0-9]\d*(\.\d+)?$/]');
            $this->form_validation->set_rules('vendor', 'vendor', 'trim|callback_vendor_check');
            if ($this->form_validation->run() == false) {
                $data["active"] = "products";
                $data["roles"] = $this->common->get_all("designations");
                $data["vendors"] = $this->common->get_one_item_where("delete_status", 1, "vendors");
                $this->load->view("admin/sales/add_new_product", $data);
            } else {
                $data = $this->input->post(array('name', 'type', 'unit_type', 'role', 'price', 'vendor'));
                $data["created_ip"] = $this->input->ip_address();
                $insert_id = $this->common->item_insert("product_services", $data);
                if ($insert_id) {
                    $data = array("encrypt_id" => $this->common->get_hash($insert_id));
                    $this->common->update_item("id", $insert_id, "product_services", $data);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/sales/products_services/view_products_services');
            }
        }
    }

    /**************edit form for product/service************/
    public function edit_product_service($encrypt_id)
    {
        $data["active"] = "products";
        $data["product_service"] = $this->common->get_one_item_where("encrypt_id", $encrypt_id, "product_services");
        $data["roles"] = $this->common->get_all("designations");
        $data["vendors"] = $this->common->get_all("vendors");
        $this->load->view("admin/sales/edit_product_service", $data);
    }

    /**************editing product/service************/
    public function editing_product_service()
    {
        if (isset($_POST["submit"])) {
            $encrypt_id = $this->input->post('id');
            $this->form_validation->set_rules('name', 'name', 'trim|required|max_length[60]');
            $this->form_validation->set_rules('type', 'type', 'trim|required|callback_type_check');
            $this->form_validation->set_rules('unit_type', 'unit type', 'trim|required');
            $this->form_validation->set_rules('price', 'price', 'trim|required|regex_match[/^-?[0-9]\d*(\.\d+)?$/]');
            $this->form_validation->set_rules('vendor', 'vendor', 'trim|callback_vendor_check');
            if ($this->form_validation->run() == false) {
                $data["active"] = "products";
                $data["product_service"] = $this->common->get_one_item_where("encrypt_id", $encrypt_id, "product_services");
                $data["roles"] = $this->common->get_all("designations");
                $data["vendors"] = $this->common->get_all("vendors");
                $this->load->view("admin/sales/edit_product_service", $data);
            } else {
                $data = $this->input->post(array('name', 'type', 'unit_type', 'role', 'price', 'vendor'));
                $affected_rows = $this->common->update_item("encrypt_id", $encrypt_id, "product_services", $data);
                if ($affected_rows) {
                    $data["updated_ip"] = $this->input->ip_address();
                    $data["updated_at"] = date("y-m-d H:i:s");
                    $this->common->update_item("encrypt_id", $encrypt_id, "product_services", $data);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/sales/products_services/view_products_services');

            }
        }
    }

    /**************delete product_details************/
    public function delete_products_services($encrypt_id)
    {
        $data = array("delete_status" => 0);
        $affected_rows = $this->common->update_item("encrypt_id", $encrypt_id, "product_services", $data);
        if ($affected_rows) {
            $this->session->set_flashdata('success_msg', 'Deleted successfully...!!!');
        } else {
            $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
        }
        redirect('admins/sales/contacts/view_all_contacts');
    }

    /**************view product_details************/
    public function view_product_details($encrypt_id)
    {
        $data["active"] = "products";
        $data["product_service"] = $this->common->get_one_item_where("encrypt_id", $encrypt_id, "product_services");
        $data["role"] = $this->common->get_one_item_where("encrypt_id", $data["product_service"][0]->role, "designations");
        $data["vendors"] = $this->common->get_one_item_where("encrypt_id", $data["product_service"][0]->vendor, "vendors");
       $this->load->view("admin/sales/view_product_details", $data);
    }

    /**************check correct value for gender************/
    public function type_check($val)
    {
        if ($val == 1 || $val == 2) { //1=>product 2=>service no other values are possible
            return TRUE;
        } else {
            $this->form_validation->set_message('type_check', 'The {field} field is not correct value');
            return FALSE;
        }
    }

    /**************check correct value for gender************/
    public function unit_type_check($val)
    {
        if ($val == "Day" || $val == "Hr" || $val = "No") { //1=>product 2=>service no other values are possible
            return TRUE;
        } else {
            $this->form_validation->set_message('unit_type_check', 'The {field} field is not correct value');
            return FALSE;
        }
    }

    /**************check correct value for vendor************/
    public function vendor_check($val)
    {
        if ($val != "") {
            $get_designation = $this->common->get_two_item_where("encrypt_id", $val, "delete_status", 1, "vendors");
            if (empty($get_designation)) { //1=>male 2=>female no other values are possible
                $this->form_validation->set_message('vendor_check', 'The {field} field is not correct value');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return TRUE;

        }

    }
}
