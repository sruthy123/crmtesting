<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class People extends CI_Controller
{
    /**************constructor************/
    function __construct()
    {
        parent::__construct();
        $this->load->model('common');
        $this->common->check_session();
    }

    /**************load people view page************/
    public function view_people()
    {
        $data["active"] = "people";
        $data["employees"] = $this->common->get_three_table_join_one_where("employee_basic_details.*,employee_work_details.*,designations.designation", "employee_work_details", "employee_basic_details.encrypt_id=employee_work_details.employee_id", "designations", "employee_work_details.designation_id=designations.encrypt_id", "employee_basic_details", "employee_basic_details.delete_status", 1);
        $this->load->view('admin/hrms/view_people', $data);
    }

    /**************load people view page************/
    public function add_people()
    {
        $data["active"] = "people";
        $data["designations"] = $this->common->get_one_item_where("delete_status", 1, "designations");
        $this->load->view('admin/hrms/add_people', $data);
    }

    /**************adding people************/
    public function adding_people()
    {
        $data["active"] = "people";
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('first_name', 'First name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('gender', 'gender', 'trim|required|callback_gender_check');
            $this->form_validation->set_rules('e_mail', 'E-Mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('address', 'address', 'trim|required');
            $this->form_validation->set_rules('department', 'department', 'trim|required|callback_department_check');
            $this->form_validation->set_rules('date_of_joining', 'Date of joining', 'trim|required|regex_match[/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/]');
            $this->form_validation->set_rules('designation_id', 'designation', 'trim|required|callback_designation_check');
            if ($this->form_validation->run() == false) {
                $data["designations"] = $this->common->get_one_item_where("delete_status", 1, "designations");
                $this->load->view('admin/hrms/add_people', $data);
            } else {
                $data = $this->input->post(array('first_name', 'last_name', 'gender', 'e_mail', 'phone', 'address'));
                $data["created_ip"] = $this->input->ip_address();
                $insert_id = $this->common->item_insert("employee_basic_details", $data);
                if ($insert_id) {
                    $encrypt_id=$this->common->get_hash($insert_id.date("Y-m-d H:i:s"));
                    $data = array(
                        "encrypt_id" =>$encrypt_id
                    );
                    $this->common->update_item("id", $insert_id, "employee_basic_details", $data);
                    $data=$this->input->post(array('department','date_of_joining','designation_id'));
                    $data["employee_id"]=$encrypt_id;
                    $insert_id = $this->common->item_insert("employee_work_details", $data);
                    if ($insert_id) {
                        $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/hrms/people/view_people');
            }
        }
    }

    /**************view  people details************/
    public function view_people_details($encrypt_id)
    {
        $data["active"] = "people";
        $data["employee"] = $this->common->get_three_table_join_one_where("employee_basic_details.*,employee_work_details.*,designations.designation", "employee_work_details", "employee_basic_details.encrypt_id=employee_work_details.employee_id", "designations", "employee_work_details.designation_id=designations.encrypt_id", "employee_basic_details", "employee_basic_details.encrypt_id", $encrypt_id);
        $this->load->view('admin/hrms/view_people_details', $data);
    }

    /**************edit  people details************/
    public function edit_people_details($encrypt_id)
    {
        $data["active"] = "people";
        $data["employee"] = $this->common->get_three_table_join_one_where("employee_basic_details.*,employee_work_details.*,designations.designation,designations.encrypt_id as designation_id", "employee_work_details", "employee_basic_details.encrypt_id=employee_work_details.employee_id", "designations", "employee_work_details.designation_id=designations.encrypt_id", "employee_basic_details", "employee_basic_details.encrypt_id", $encrypt_id);
        $data["designations"] = $this->common->get_one_item_where("delete_status", 1, "designations");
        $this->load->view('admin/hrms/edit_people_details', $data);
    }

    /**************editing  people details************/
    public function editing_people_details()
    {
        if (isset($_POST["submit"])) {
            $this->form_validation->set_rules('first_name', 'First name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha|max_length[30]');
            $this->form_validation->set_rules('gender', 'gender', 'trim|required|callback_gender_check');
            $this->form_validation->set_rules('e_mail', 'E-Mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('address', 'address', 'trim|required');
            $this->form_validation->set_rules('department', 'department', 'trim|required|callback_department_check');
            $this->form_validation->set_rules('date_of_joining', 'Date of joining', 'trim|required|regex_match[/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/]');
            $this->form_validation->set_rules('designation_id', 'designation', 'trim|required|callback_designation_check');
            if ($this->form_validation->run() == false) {
                $data["active"] = "people";
                $data["employee"] = $this->common->get_three_table_join_one_where("employee_basic_details.*,employee_work_details.*,designations.designation,designations.encrypt_id as designation_id", "employee_work_details", "employee_basic_details.encrypt_id=employee_work_details.employee_id", "designations", "employee_work_details.designation_id=designations.encrypt_id", "employee_basic_details", "employee_basic_details.encrypt_id", $this->input->post('encrypt_id'));
                $data["designations"] = $this->common->get_one_item_where("delete_status", 1, "designations");
                $this->load->view('admin/hrms/edit_people_details', $data);
            } else {
                $data = $this->input->post(array('first_name', 'last_name', 'gender', 'e_mail', 'phone', 'address'));
                $affected_rows_basic_details = $this->common->update_item("encrypt_id", $this->input->post('id'), "employee_basic_details", $data);
                $data=$this->input->post(array('department','date_of_joining','designation_id'));
                $affected_rows_work_details = $this->common->update_item("employee_id", $this->input->post('id'), "employee_work_details", $data);
                if ($affected_rows_basic_details || $affected_rows_work_details) {
                    $datas["updated_at"]=date("Y-m-d H:i:s");
                    $this->common->update_item("encrypt_id", $this->input->post('id'), "employee_basic_details", $datas);
                    $this->session->set_flashdata('success_msg', 'Saved successfully...!!!');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
                }
                redirect('admins/hrms/people/view_people');
            }
        }
    }

    /**************delete employee details************/
    public function delete_employee($encrypt_id)
    {
        $data = array("delete_status" => 0);
        $affected_rows = $this->common->update_item("encrypt_id", $encrypt_id, "employee_basic_details", $data);
        if ($affected_rows) {
            $this->session->set_flashdata('success_msg', 'Deleted successfully...!!!');
        } else {
            $this->session->set_flashdata('error_msg', 'Failed.Try agaain...!!!');
        }
        redirect('admins/hrms/people/view_people');
    }

    /**************check correct value for gender************/
    public function gender_check($val)
    {
        if ($val == 1 || $val == 2) { //1=>male 2=>female no other values are possible
            return TRUE;
        } else {
            $this->form_validation->set_message('gender_check', 'The {field} field is not correct value');
            return FALSE;
        }
    }

    /**************check correct value for department************/
    public function department_check($val)
    {
        if ($val == "Sales" || $val == "Marketing" || $val == "HR" || $val == "Delivery") { //1=>male 2=>female no other values are possible
            return TRUE;
        } else {
            $this->form_validation->set_message('gender_check', 'The {field} field is not correct value');
            return FALSE;
        }
    }

    /**************check correct value for department************/
    public function designation_check($val)
    {
        $get_designation = $this->common->get_one_item_where("encrypt_id", $val, "designations");
        if (empty($get_designation)) { //1=>male 2=>female no other values are possible
            $this->form_validation->set_message('gender_check', 'The {field} field is not correct value');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
