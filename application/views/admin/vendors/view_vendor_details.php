<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Admin</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/highlightjs/github.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>public/admin/css/bracket.css">
</head>

<body>

<?php $this->load->view('admin/menu'); ?>

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">

    </div>

    <div class="br-pagebody">
        <div class="br-section-wrapper">


            <div class="pd-30 bd">

                <dl class="row">
                    <dt class="col-sm-12 tx-inverse">VENDOR DETAILS</dt>


                    <dt class="col-sm-3 tx-inverse">First Name</dt>
                    <dd class="col-sm-9"><?=$vendor[0]->first_name;?></dd>

                    <dt class="col-sm-3 tx-inverse">Last Name</dt>
                    <dd class="col-sm-9"><?=$vendor[0]->last_name;?></dd>

                    <dt class="col-sm-3 tx-inverse">gender</dt>
                    <dd class="col-sm-9"><?=($vendor[0]->gender==1)?"Male":"Female";?></dd>

                    <dt class="col-sm-3 tx-inverse">E-Mail</dt>
                    <dd class="col-sm-9"><?=$vendor[0]->e_mail;?></dd>
                    <dt class="col-sm-3 tx-inverse">Phone no</dt>
                    <dd class="col-sm-9"><?=$vendor[0]->phone;?></dd>
                    <dt class="col-sm-3 text-truncate tx-inverse">Address</dt>
                    <dd class="col-sm-9"><?=$vendor[0]->address;?></dd>

                    <dt class="col-sm-3 tx-inverse"></dt>
                    <dd class="col-sm-9">
                        <dl class="row">
                            <dd class="col-sm-8">
                                <a href="<?= base_url() ?>admins/vendors/vendors/edit_vendor_details/<?=$vendor[0]->encrypt_id?>"
                                   class="btn btn-success btn-with-icon">
                                    <div class="ht-40 justify-content-between">
                                        <span class="icon wd-40"><i class="fa fa-edit"></i></span>
                                    </div>
                                </a>
                                <a class="btn btn-danger btn-with-icon remove" data-encrypt_id="<?=$vendor[0]->encrypt_id;?>">
                                    <div class="ht-40 justify-content-between">
                                        <span class="icon wd-40"><i class="fa fa-trash"></i></span>
                                    </div>
                                </a>
                            </dd>

                        </dl>
                    </dd>

                </dl>
            </div>

        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2017. Bracket. All Rights Reserved.</div>
            <div>Attentively and carefully made by ThemePixels.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5"
               href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5"
               href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-twitter tx-20"></i></a>
        </div>
    </footer>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="<?= base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?= base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?= base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url() ?>public/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/moment/moment.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?= base_url() ?>public/admin/lib/peity/jquery.peity.js"></script>
<script src="<?= base_url() ?>public/admin/lib/highlightjs/highlight.pack.js"></script>

<script src="<?= base_url() ?>public/admin/js/bracket.js"></script>
<!---sruthy--->
<!---removed because no such a file create 404 in js--->
<!--<script src="--><? //= base_url() ?><!--public/admin/js/tooltip-colored.js"></script>-->
<!--<script src="--><? //= base_url() ?><!--public/admin/js/popover-colored.js"></script>-->
<script>
    $(".remove").click(function(){
        if(confirm('Are you sure to remove this item ?'))
        {
            var encrypt_id=$(this).data("encrypt_id");
            window.location.href ="<?= base_url() ?>admins/vendors/vendors/delete_vendor/"+encrypt_id;
        }
    });
</script>
</body>
</html>
