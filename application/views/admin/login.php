<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Admin</title>

    <!-- vendor css -->
    <link href="<?php echo base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>public/admin/css/bracket.css">
    <!----sruthy--->
    <!--toastr--->
    <link href="<?php echo base_url()?>public/toastr-master/build/toastr.css" rel="stylesheet" type="text/css" />
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <style>
        span{
            color: red;
        }
    </style>
    <!----sruthy--->

</head>

<body>

<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
    <form method="post" action="<?php echo base_url()?>admins/check_login">
<!--        --><?php
//        $csrf = array(
//            'name' => $this->security->get_csrf_token_name(),
//            'hash' => $this->security->get_csrf_hash()
//        );
//        ?>
<!--        <input type="hidden" name="--><?//=$csrf['name'];?><!--" value="--><?//=$csrf['hash'];?><!--" />-->

        <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
            <div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><span class="tx-normal"></span> Login <span
                        class="tx-normal"></span></div>
            <!--        <div class="tx-center mg-b-60">The Admin Template For Perfectionist</div>-->

            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Enter  username" value="<?php echo set_value('username'); ?>">

            </div><!-- form-group -->
            <span><?php echo form_error('username'); ?></span>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Enter  password" value="<?php echo set_value('password'); ?>">
                <!--          <a href="" class="tx-info tx-12 d-block mg-t-10">Forgot password?</a>-->
            </div><!-- form-group -->
            <span><?php echo form_error('password'); ?></span>
            <button type="submit" name="submit" class="btn btn-info btn-block">Login</button>

            <!--        <div class="mg-t-60 tx-center">Not yet a member? <a href="" class="tx-info">Sign Up</a></div>-->
        </div><!-- login-wrapper -->
    </form>
</div><!-- d-flex -->

<script src="<?php echo base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?php echo base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?php echo base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<!--toast-->
<script src="<?php echo base_url()?>public/toastr-master/toastr.js"></script>
<script>
    var error_msg="<?php echo $this->session->flashdata('error_msg');?>";
    if(error_msg!=""){
        toastr.error( error_msg, { positionClass: 'toast-top-right', timeOut: -1 })
    }
</script>
</body>
</html>
