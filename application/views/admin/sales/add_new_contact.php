<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Form Layouts Design - Bracket Responsive Bootstrap 4 Admin Template</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/highlightjs/github.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>public/admin/css/bracket.css">
</head>

<body>

<?php $this->load->view('admin/menu'); ?>

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">

    </div>

    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Add Contact</h6>
            <form method="post" action="<?=base_url()?>admins/sales/contacts/adding_new_contact">
                <div class="form-layout form-layout-1">
                    <div class="row mg-b-25">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Mr/ Mrs/ Miss <span class="tx-danger">*</span></label>
                                <select class="form-control select2" name="prefix" >
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Firstname: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="first_name"
                                       placeholder="Enter firstname" value="<?=set_value('first_name')?>">
                                <span class="tx-danger"><?= form_error('first_name')?></span>

                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Lastname: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="last_name"
                                       placeholder="Enter lastname" value="<?=set_value('last_name')?>">
                                <span class="tx-danger"><?= form_error('last_name')?></span>

                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Title: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="tittle"
                                       placeholder="Enter Title" value="<?=set_value('tittle')?>">
                                <span class="tx-danger"><?= form_error('tittle')?></span>

                            </div>
                        </div><!-- col-8 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Phone no: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="phone"
                                       placeholder="Enter Phone" value="<?=set_value('phone')?>">
                                <span class="tx-danger"><?= form_error('phone')?></span>

                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Email address: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="e_mail"
                                       placeholder="Enter email address" value="<?=set_value('e_mail')?>">
                                <span class="tx-danger"><?= form_error('e_mail')?></span>

                            </div>
                        </div><!-- col-4 -->


                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Website: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="website"
                                       placeholder="Enter Website" value="<?=set_value('website')?>">
                                <span class="tx-danger"><?= form_error('website')?></span>

                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Company: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="company"
                                       placeholder="Enter Company" value="<?=$company_details[0]->company;?>" disabled>
                                <input type="hidden" name="company_id" value="<?=$company_details[0]->encrypt_id;?>">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Department: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" data-placeholder="Choose Department" name="department">
                                    <option label="Choose Department"></option>
                                    <option label="Sales">Sales</option>
                                    <option label="Marketing">Marketing</option>
                                    <option label="HR">HR</option>
                                    <option label="Delivery">Delivery</option>
                                </select>
                            </div>
                            <span class="tx-danger"><?= form_error('department')?></span>

                        </div><!-- col-4 -->
                    </div><!-- row -->

                    <div class="form-layout-footer">
                        <button class="btn btn-info" type="submit" name="submit">Submit </button>
                        <button class="btn btn-secondary" type="reset">Cancel</button>
                    </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
            </form>

        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2017. Bracket. All Rights Reserved.</div>
            <div>Attentively and carefully made by ThemePixels.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5"
               href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5"
               href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-twitter tx-20"></i></a>
        </div>
    </footer>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<div id="modaldemo" class="modal fade effect-scale">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bd-0 rounded-0">
            <div class="modal-body pd-0">
                <div class="row flex-row-reverse no-gutters">
                    <div class="col-lg-6">
                        <div class="pd-30">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="pd-xs-x-30 pd-y-10">
                                <h5 class="tx-xs-28 tx-inverse mg-b-5">Welcome back!</h5>
                                <p>Sign in to your account to continue</p>
                                <br>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control pd-y-12"
                                           placeholder="Enter your email">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-20">
                                    <input type="email" name="password" class="form-control pd-y-12"
                                           placeholder="Enter your password">
                                    <a href="" class="tx-12 d-block mg-t-10">Forgot password?</a>
                                </div><!-- form-group -->

                                <button class="btn btn-primary pd-y-12 btn-block">Sign In</button>

                                <div class="mg-t-30 mg-b-20 tx-12">Don't have an account yet? <a href="">Sign Up</a>
                                </div>
                            </div>
                        </div><!-- pd-20 -->
                    </div><!-- col-6 -->
                    <div class="col-lg-6 bg-primary">
                        <div class="pd-40">
                            <h4 class="tx-white mg-b-20"><span>[</span> bracket + <span>]</span></h4>
                            <p class="tx-white op-7 mg-b-60">It is a long established fact that a reader will be
                                distracted by the readable content of a page when looking at its layout.</p>
                            <p class="tx-white tx-13">
                                <span class="tx-uppercase tx-medium d-block mg-b-15">Our Address:</span>
                                <span class="op-7">Ayala Center, Cebu Business Park, Cebu City, Cebu, Philippines 6000</span>
                            </p>
                        </div>
                    </div><!-- col-6 -->
                </div><!-- row -->
            </div><!-- modal-body -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script src="<?= base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?= base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url() ?>public/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/moment/moment.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?= base_url() ?>public/admin/lib/peity/jquery.peity.js"></script>
<script src="<?= base_url() ?>public/admin/lib/highlightjs/highlight.pack.js"></script>
<script src="<?= base_url() ?>public/admin/lib/select2/js/select2.min.js"></script>

<script src="<?= base_url() ?>public/admin/js/bracket.js"></script>
<script>
    $(function () {
        'use strict'

        $('.form-layout .form-control').on('focusin', function () {
            $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.form-layout .form-control').on('focusout', function () {
            $(this).closest('.form-group').removeClass('form-group-active');
        });

        // Select2
        $('#select2-a, #select2-b').select2({
            minimumResultsForSearch: Infinity
        });

        $('#select2-a').on('select2:opening', function (e) {
            $(this).closest('.form-group').addClass('form-group-active');
        });

        $('#select2-a').on('select2:closing', function (e) {
            $(this).closest('.form-group').removeClass('form-group-active');
        });

    });
</script>
</body>
</html>
