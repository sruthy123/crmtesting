<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Form Wizards Design - Bracket Responsive Bootstrap 4 Admin Template</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/highlightjs/github.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery.steps/jquery.steps.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>public/admin/css/bracket.css">
</head>

<body>

<?php $this->load->view('admin/menu'); ?>

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="br-pageheader pd-y-15 pd-l-20">
        <!--<nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="index.html">Bracket</a>
            <a class="breadcrumb-item" href="#">Forms</a>
            <span class="breadcrumb-item active">Form Wizards</span>
        </nav>-->
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <!--        <h4 class="tx-gray-800 mg-b-5">Form Wizards</h4>-->
        <!--        <p class="mg-b-0">jQuery Steps, an all-in-one wizard plugin that is extremely flexible, compact and-->
        <!--            feature-rich.</p>-->
    </div>

    <div class="br-pagebody">
        <div class="br-section-wrapper">


            <div id="wizard2">
                <h3>Personal Information</h3>
                <section>
                    <!--                    <p>Try the keyboard navigation by clicking arrow left or right!</p>-->

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group wd-xs-300">

                                    <label class="form-control-label">Customer Type: <span
                                                class="tx-danger">*</span></label>
                                    <select class="form-control select2" data-placeholder="Choose Customer Type">
                                        <option label="Choose Customer Type"></option>
                                        <option value="USA">Customer</option>
                                        <option value="UK">Partner</option>
                                        <option value="China">Reseller</option>
                                    </select>
                                </div>
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Company: <span class="tx-danger">*</span></label>
                                    <input id="firstname" class="form-control" name="firstname"
                                           placeholder="Enter company"
                                           type="text" >
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Parent Account:</label>
                                    <input id="lastname" class="form-control" name="lastname"
                                           placeholder="Enter Parent Account"
                                           type="text">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Phone: <span class="tx-danger">*</span></label>
                                    <input id="lastname" class="form-control" name="lastname" placeholder="Enter Phone"
                                           type="text">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Website: <span class="tx-danger">*</span></label>
                                    <input id="lastname" class="form-control" name="lastname" placeholder="Enter Phone"
                                           type="text">
                                </div><!-- form-group -->
                                <!--                                <div class="form-group wd-xs-300">-->
                                <!--                                    <label class="form-control-label">Groups: <span class="tx-danger">*</span></label>-->
                                <!--                                    <input id="lastname" class="form-control" name="lastname" placeholder="Enter Groups"-->
                                <!--                                           type="text">-->
                                <!--                                </div>-->
                                <!-- form-group -->
                                <div class="form-group wd-xs-300">

                                    <label class="form-control-label">Currency: <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" data-placeholder="Choose Currency">
                                        <option label="Choose Currency"></option>
                                        <option value="USA">United States of America</option>
                                        <option value="UK">United Kingdom</option>
                                        <option value="China">China</option>
                                        <option value="Japan">Japan</option>
                                    </select>
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Default Language: <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" data-placeholder="Choose Default Language">
                                        <option label="Choose Default Language"></option>
                                        <option value="USA">United States of America</option>
                                        <option value="UK">United Kingdom</option>
                                        <option value="China">China</option>
                                        <option value="Japan">Japan</option>
                                    </select>
                                </div><!-- form-group -->

                            </div>
                            <div class="col-xs-4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <div class="col-xs-4">


                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Address: <span class="tx-danger">*</span></label>
                                    <textarea rows="3" class="form-control" placeholder="Address"></textarea>

                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">City: <span
                                                class="tx-danger">*</span></label>
                                    <input id="lastname" class="form-control" name="lastname"
                                           placeholder="City"
                                           type="text">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">State: <span class="tx-danger">*</span></label>
                                    <input id="lastname" class="form-control" name="lastname" placeholder="Enter State"
                                           type="text">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Zip code: <span class="tx-danger">*</span></label>
                                    <input id="lastname" class="form-control" name="lastname"
                                           placeholder="Enter Zip code"
                                           type="text">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Country: <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" data-placeholder="Choose Country">
                                        <option label="Choose Country"></option>
                                        <option value="USA">United States of America</option>
                                        <option value="UK">United Kingdom</option>
                                        <option value="China">China</option>
                                        <option value="Japan">Japan</option>
                                    </select>
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Business unit: <span class="tx-danger">*</span></label>
                                    <input  class="form-control" name="firstname"
                                           placeholder="DigitXp"
                                           type="text" disabled>
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Territory: <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" data-placeholder="Choose Territory" disabled>
                                        <option label="Kerala"></option>

                                    </select>
                                </div><!-- form-group -->

                            </div>
                        </div>
                    </div>

                </section>
                <h3>Billing Address</h3>
                <section>
                    <div class="col-xs-4">
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">Street: <span class="tx-danger">*</span></label>
                            <input id="lastname" class="form-control" name="lastname"
                                   placeholder="Street"
                                   type="text">
                        </div><!-- form-group -->
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">City: <span
                                        class="tx-danger">*</span></label>
                            <input id="lastname" class="form-control" name="lastname"
                                   placeholder="City"
                                   type="text">
                        </div><!-- form-group -->
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">State: <span class="tx-danger">*</span></label>
                            <input id="lastname" class="form-control" name="lastname" placeholder="Enter State"
                                   type="text">
                        </div><!-- form-group -->
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">Zip code: <span class="tx-danger">*</span></label>
                            <input id="lastname" class="form-control" name="lastname" placeholder="Enter Zip code"
                                   type="text">
                        </div><!-- form-group -->
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">Country: <span class="tx-danger">*</span></label>
                            <select class="form-control select2" data-placeholder="Choose Country">
                                <option label="Choose Country"></option>
                                <option value="USA">United States of America</option>
                                <option value="UK">United Kingdom</option>
                                <option value="China">China</option>
                                <option value="Japan">Japan</option>
                            </select>
                        </div><!-- form-group -->

                    </div>
                </section>
                <h3>Shipping Address</h3>
                <section>
                    <div class="col-xs-4">
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">Street: </label>
                            <input id="lastname" class="form-control" name="lastname"
                                   placeholder="Street"
                                   type="text">
                        </div><!-- form-group -->
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">City: </label>
                            <input id="lastname" class="form-control" name="lastname"
                                   placeholder="City"
                                   type="text">
                        </div><!-- form-group -->
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">State: </label>
                            <input id="lastname" class="form-control" name="lastname" placeholder="Enter State"
                                   type="text">
                        </div><!-- form-group -->
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">Zip code: </label>
                            <input id="lastname" class="form-control" name="lastname" placeholder="Enter Zip code"
                                   type="text">
                        </div><!-- form-group -->
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">Country: </label>
                            <select class="form-control select2" data-placeholder="Choose country">
                                <option label="Choose country"></option>
                                <option value="USA">United States of America</option>
                                <option value="UK">United Kingdom</option>
                                <option value="China">China</option>
                                <option value="Japan">Japan</option>
                            </select>
                        </div><!-- form-group -->

                    </div>
                </section>
            </div>


        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2017. Bracket. All Rights Reserved.</div>
            <div>Attentively and carefully made by ThemePixels.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5"
               href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5"
               href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-twitter tx-20"></i></a>
        </div>
    </footer>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="<?= base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?= base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url() ?>public/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/moment/moment.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?= base_url() ?>public/admin/lib/peity/jquery.peity.js"></script>
<script src="<?= base_url() ?>public/admin/lib/highlightjs/highlight.pack.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery.steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>public/admin/lib/parsleyjs/parsley.js"></script>

<script src="<?= base_url() ?>public/admin/js/bracket.js"></script>
<script>
    $(document).ready(function () {
        'use strict';

        $('#wizard1').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>'
        });

        $('#wizard2').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onStepChanging: function (event, currentIndex, newIndex) {
                if (currentIndex < newIndex) {
                    // Step 1 form validation
                    var fname = $('#firstname').parsley();
                    if(fname.isValid() ) {
                        return true;
                    }
                else {
                    fname.validate();
                }
                }
            }
        });

        $('#wizard3').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            stepsOrientation: 1
        });

        $('#wizard4').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            cssClass: 'wizard step-equal-width'
        });

        $('#wizard5').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            cssClass: 'wizard wizard-style-1'
        });

        $('#wizard6').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            cssClass: 'wizard wizard-style-2'
        });

        $('#wizard7').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            cssClass: 'wizard wizard-style-3'
        });

    });
</script>
</body>
</html>
