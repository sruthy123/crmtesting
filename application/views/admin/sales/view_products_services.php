<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Admin</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/highlightjs/github.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>public/admin/css/bracket.css">
    <!--toastr--->
    <link href="<?php echo base_url() ?>public/toastr-master/build/toastr.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<?php $this->load->view('admin/menu'); ?>

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">



    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="row">


            <div class="col-sm-6 col-md-2 mg-t-20 mg-md-t-0">
                <div class="btn-demo">
                    <button class="btn btn-outline-primary active btn-block mg-b-10" onclick="window.location.href='<?=base_url()?>admins/sales/products_services/add_new_product'">Add New</button>

                </div><!-- btn-demo -->
            </div><!-- col-sm-3 -->

        </div><!-- row -->


    </div>


    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Products	/ Services</h6>
            <!--            <p class="mg-b-25 mg-lg-b-50">Searching, ordering and paging goodness will be immediately added to the-->
            <!--                table, as shown in this example.</p>-->

            <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-15p">#</th>
                        <th class="wd-15p">Type</th>
                        <th class="wd-15p">Name</th>
                        <th class="wd-15p">Price</th>

                        <th class="wd-25p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($product_services as $key=>$product_service){?>
                        <tr>
                            <td><?=$key+1;?></td>
                            <td><?=($product_service->type==1)?"Product":"Service";?></td>
                            <td><?=$product_service->name;?> </td>
                            <td><?=$product_service->price;?></td>

                            <td>
                                <a href="<?=base_url()?>admins/sales/products_services/view_product_details/<?=$product_service->encrypt_id;?>" class="btn btn-primary btn-with-icon">
                                    <div class="ht-40 justify-content-between">
                                        <span class="icon wd-40"><i class="fa fa-eye"></i></span>
                                    </div>
                                </a>

                            </td>
                        </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div><!-- table-wrapper -->


        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2017. Bracket. All Rights Reserved.</div>
            <div>Attentively and carefully made by ThemePixels.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5"
               href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5"
               href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-twitter tx-20"></i></a>
        </div>
    </footer>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="<?= base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?= base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url() ?>public/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/moment/moment.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?= base_url() ?>public/admin/lib/peity/jquery.peity.js"></script>
<script src="<?= base_url() ?>public/admin/lib/highlightjs/highlight.pack.js"></script>
<script src="<?= base_url() ?>public/admin/lib/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>public/admin/lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="<?= base_url() ?>public/admin/lib/select2/js/select2.min.js"></script>

<script src="<?= base_url() ?>public/admin/js/bracket.js"></script>
<script>
    $(function () {
        'use strict';

        $('#datatable1').DataTable({
            responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        $('#datatable2').DataTable({
            bLengthChange: false,
            searching: false,
            responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});

    });
</script>
<!---toastr script--->
<script src="<?php echo base_url() ?>public/toastr-master/toastr.js"></script>
<script>
    var error_msg = "<?php echo $this->session->flashdata('error_msg');?>";
    if (error_msg != "") {
        toastr.error(error_msg, {positionClass: 'toast-top-right', timeOut: -1})
    }
    var success_msg = "<?php echo $this->session->flashdata('success_msg');?>";
    if (success_msg != "") {
        toastr.success(success_msg, {positionClass: 'toast-top-right', timeOut: -1})
    }
</script>

</body>
</html>
