<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Admin</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/highlightjs/github.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery.steps/jquery.steps.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>public/admin/css/bracket.css">
    <!--toastr--->
    <link href="<?php echo base_url() ?>public/toastr-master/build/toastr.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<?php $this->load->view('admin/menu'); ?>


<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">

    </div>

    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <form method="post" action="<?=base_url()?>admins/sales/customers/adding_customer">
                <div id="wizard1">
                        <h3>Personal Information</h3>
                        <section>
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Customer Type: <span
                                                        class="tx-danger">*</span></label>
                                            <select class="form-control select2" data-placeholder="Choose Customer Type"
                                                    name="customer_type">
                                                <option label="Choose Customer Type"></option>
                                                <option value="Customer">Customer</option>
                                                <option value="Partner">Partner</option>
                                                <option value="Reseller">Reseller</option>
                                            </select>

                                            <span class="tx-danger"><?= form_error('customer_type')?></span>
                                        </div>
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Company: <span class="tx-danger">*</span></label>
                                            <input  class="form-control" name="company"
                                                   placeholder="Enter company"
                                                   type="text" value="<?=set_value('company')?>">
                                            <span class="tx-danger"><?= form_error('company')?></span>
                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Parent Account:</label>
                                            <input
                                                    class="form-control" name="parent_account"
                                                   placeholder="Enter Parent Account"
                                                   type="text" value="<?=set_value('parent_account')?>">
                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Phone: <span class="tx-danger">*</span></label>
                                            <input class="form-control" name="phone" placeholder="Enter Phone"
                                                   type="text" value="<?= set_value('phone')?>" >
                                            <span class="tx-danger"><?= form_error('phone')?></span>
                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">E-mail: <span class="tx-danger">*</span></label>
                                            <input class="form-control" name="e_mail" placeholder="Enter E-mail"
                                                   type="text" value="<?= set_value('e_mail')?>" >
                                            <span class="tx-danger"><?= form_error('e_mail')?></span>
                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Website: <span class="tx-danger">*</span></label>
                                            <input id="lastname" class="form-control" name="website" placeholder="Enter Website"
                                                   type="text" value="<?=set_value('website')?>">
                                            <span class="tx-danger"><?= form_error('website')?></span>
                                        </div>
                                        <div class="form-group wd-xs-300">

                                            <label class="form-control-label">Currency: <span class="tx-danger">*</span></label>
                                            <select class="form-control select2" data-placeholder="Choose Currency" name="currency">
                                                <option label="Choose Currency"></option>
                                                <?php foreach ($currencies as $currency){?>
                                                    <option value="<?= $currency->id?>"><?= $currency->name?></option>
                                                <?php }?>
                                            </select>
                                            <span class="tx-danger"><?= form_error('currency')?></span>
                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Default Language: <span class="tx-danger">*</span></label>
                                            <select class="form-control select2" data-placeholder="Choose Default Language" name="default_language">
                                                <option value="">System Default</option>
                                                <?php foreach ($default_languages as $default_language){?>
                                                    <option value="<?= $default_language->id?>"><?= $default_language->language?></option>
                                                <?php }?>
                                            </select>
                                            <span class="tx-danger"><?= form_error('default_language')?></span>
                                        </div><!-- form-group -->

                                    </div>
                                    <div class="col-xs-4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                    <div class="col-xs-4">
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Address: <span class="tx-danger">*</span></label>
                                            <textarea rows="3" class="form-control" placeholder="Address" name="address"><?=set_value('address')?></textarea>
                                            <span class="tx-danger"><?= form_error('address')?></span>
                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">City: <span
                                                        class="tx-danger">*</span></label>
                                            <input  class="form-control" name="city"
                                                   placeholder="City"
                                                   type="text" value="<?=set_value('city')?>">
                                            <span class="tx-danger"><?= form_error('city')?></span>

                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">State: <span class="tx-danger">*</span></label>
                                            <input  class="form-control" name="state" placeholder="Enter State"
                                                   type="text" value="<?=set_value('state')?>">
                                            <span class="tx-danger"><?= form_error('state')?></span>

                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Zip code: <span class="tx-danger">*</span></label>
                                            <input  class="form-control" name="zip_code"
                                                   placeholder="Enter Zip code"
                                                   type="text" value="<?=set_value('zip_code')?>">
                                            <span class="tx-danger"><?= form_error('zip_code')?></span>
                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Country: <span class="tx-danger">*</span></label>
                                            <select class="form-control select2" data-placeholder="Choose Country" name="country_id">
                                                <option label="Choose Country"></option>
                                                <?php foreach ($countries as $country){?>
                                                    <option value="<?= $country->country_id?>"><?= $country->long_name?></option>
                                                <?php }?>
                                            </select>
                                            <span class="tx-danger"><?= form_error('country_id')?></span>
                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Business unit: <span
                                                        class="tx-danger">*</span></label>
                                            <input class="form-control" name="firstname"
                                                   placeholder="DigitXp"
                                                   type="text" disabled>

                                        </div><!-- form-group -->
                                        <div class="form-group wd-xs-300">
                                            <label class="form-control-label">Territory: <span
                                                        class="tx-danger">*</span></label>
                                            <select class="form-control select2" data-placeholder="Choose Territory" disabled>
                                                <option label="Kerala"></option>

                                            </select>
                                        </div><!-- form-group -->

                                    </div>
                                </div>
                            </div>
                        </section>
                        <h3>Billing Address</h3>
                        <section>
                            <div class="col-xs-4">
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Street: <span class="tx-danger">*</span></label>
                                    <input class="form-control" name="billing_street"
                                           placeholder="Street"
                                           type="text" value="<?=set_value('billing_street')?>">
                                    <span class="tx-danger"><?= form_error('billing_street')?></span>
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">City: <span
                                                class="tx-danger">*</span></label>
                                    <input  class="form-control" name="billing_city"
                                           placeholder="City"
                                           type="text" value="<?=set_value('billing_city')?>">
                                    <span class="tx-danger"><?= form_error('billing_city')?></span>
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">State: <span class="tx-danger">*</span></label>
                                    <input  class="form-control" name="billing_state" placeholder="Enter State"
                                           type="text" value="<?=set_value('billing_state')?>">
                                    <span class="tx-danger"><?= form_error('billing_state')?></span>
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Zip code: <span class="tx-danger">*</span></label>
                                    <input  class="form-control" name="billing_zip_code" placeholder="Enter Zip code"
                                           type="text" value="<?=set_value('billing_zip_code')?>">
                                    <span class="tx-danger"><?= form_error('billing_zip_code')?></span>
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Country: <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" data-placeholder="Choose Country" name="billing_country">
                                        <option label="Choose Country"></option>
                                        <?php foreach ($countries as $country){?>
                                            <option value="<?= $country->country_id?>"><?= $country->long_name?></option>
                                        <?php }?>
                                    </select>
                                    <span class="tx-danger"><?= form_error('billing_country')?></span>
                                </div><!-- form-group -->

                            </div>
                        </section>
                        <h3>Shipping Address</h3>
                        <section>
                            <div class="col-xs-4">
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Street: </label>
                                    <input  class="form-control" name="shipping_street"
                                           placeholder="Street"
                                           type="text" value="<?=set_value('shipping_street')?>">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">City: </label>
                                    <input  class="form-control" name="shipping_city"
                                           placeholder="City"
                                           type="text" value="<?=set_value('shipping_city')?>">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">State: </label>
                                    <input  class="form-control" name="shipping_state" placeholder="Enter State"
                                           type="text" value="<?=set_value('shipping_state')?>">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Zip code: </label>
                                    <input  class="form-control" name="shipping_zip_code" placeholder="Enter Zip code"
                                           type="text" value="<?=set_value('shipping_zip_code')?>">
                                </div><!-- form-group -->
                                <div class="form-group wd-xs-300">
                                    <label class="form-control-label">Country: </label>
                                    <select class="form-control select2" data-placeholder="Choose country" name="shipping_country">
                                        <option label="Choose country"></option>
                                        <?php foreach ($countries as $country){?>
                                            <option value="<?= $country->country_id?>"><?= $country->long_name?></option>
                                        <?php }?>
                                    </select>
                                </div><!-- form-group -->

                            </div>
                            <button type="submit" id="save" name="submit" style="display: none;"></button>
                        </section>
                </div>
            </form>


        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2017. Bracket. All Rights Reserved.</div>
            <div>Attentively and carefully made by ThemePixels.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5"
               href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5"
               href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-twitter tx-20"></i></a>
        </div>
    </footer>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="<?= base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?= base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url() ?>public/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/moment/moment.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?= base_url() ?>public/admin/lib/peity/jquery.peity.js"></script>
<script src="<?= base_url() ?>public/admin/lib/highlightjs/highlight.pack.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery.steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>public/admin/lib/parsleyjs/parsley.js"></script>

<script src="<?= base_url() ?>public/admin/js/bracket.js"></script>
<script>
    $(document).ready(function () {
        'use strict';

        $('#wizard1').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onStepChanging: function (event, currentIndex, newIndex) {
                console.log(currentIndex);

                return true;

            }
        });
        $('a[href="#finish"]').click(function(){
           $("#save").click();
        });


    });
</script>
<!-------------sruthy--------------->
<!---toastr script--->
<script src="<?php echo base_url() ?>public/toastr-master/toastr.js"></script>
<script>
    var error_msg = "<?php echo $this->session->flashdata('error_msg');?>";
    if (error_msg != "") {
        toastr.error(error_msg, {positionClass: 'toast-top-right', timeOut: -1})
    }
    var success_msg = "<?php echo $this->session->flashdata('success_msg');?>";
    if (success_msg != "") {
        toastr.success(success_msg, {positionClass: 'toast-top-right', timeOut: -1})
    }
</script>
</body>
</html>
