<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Form Layouts Design - Bracket Responsive Bootstrap 4 Admin Template</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/highlightjs/github.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>public/admin/css/bracket.css">
</head>

<body>

<?php $this->load->view('admin/menu'); ?>

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">

    </div>

    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Add Deal</h6>
            <form method="post" action="<?=base_url()?>admins/sales/deals/adding_new_deal">
                <div class="form-layout form-layout-1">
                    <div class="row mg-b-25">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Deal name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="deal_name"
                                       placeholder="Enter Deal name" value="<?=set_value('deal_name')?>">
                            </div>
                            <span class="tx-danger"><?= form_error('deal_name')?></span>

                        </div><!-- col-4 -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Deal stage: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" data-placeholder="Choose Deal stage" name="deal_stage">
                                    <option value="">Choose</option>
                                    <?php foreach ($deal_stages as $deal_stage) { ?>
                                        <option value="<?= $deal_stage->id; ?>"><?= $deal_stage->deal_stage; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <span class="tx-danger"><?= form_error('deal_stage')?></span>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Amount: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="amount"
                                       placeholder="Enter Amount" value="<?=set_value('amount')?>">
                            </div>
                            <span class="tx-danger"><?= form_error('amount')?></span>

                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Close Date: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="date" name="close_date"
                                       placeholder="Enter Phone" value="<?=set_value('close_date')?>">
                            </div>
                            <span class="tx-danger"><?= form_error('close_date')?></span>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Company: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" data-placeholder="Choose Company" name="company">
                                    <option value="">Choose</option>
                                    <?php foreach ($accounts as $account) { ?>
                                        <option value="<?= $account->encrypt_id; ?>"><?= $account->company; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <span class="tx-danger"><?= form_error('company')?></span>

                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Contact: </label>
                                <select class="form-control select2" data-placeholder="Choose Contact" name="contact" >
                                    <option value="">Choose</option>
                                    <?php foreach ($contacts as $contact) { ?>
                                        <option value="<?= $contact->encrypt_id; ?>"><?= $contact->first_name; ?>&nbsp;<?= $contact->last_name; ?></option>
                                    <?php } ?>

                                </select>
                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Next Action: </label>
                                <input class="form-control" type="text" name="next_action"
                                       placeholder="Enter Next Action" value="<?=set_value('next_action')?>">
                            </div>

                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Probability: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="probability"
                                       placeholder="Enter Probability" value="<?=set_value('probability')?>">
                            </div>
                            <span class="tx-danger"><?= form_error('probability')?></span>

                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Expected Revenue&nbsp;(in %): <span
                                            class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="expected_revenue"
                                       placeholder="Enter Expected Revenue" value="<?=set_value('expected_revenue')?>">
                            </div>
                            <span class="tx-danger"><?= form_error('expected_revenue')?></span>

                        </div><!-- col-4 -->
                    </div><!-- row -->

                    <div class="form-layout-footer">
                        <button class="btn btn-info" type="submit" name="submit">Submit</button>
                        <button class="btn btn-secondary" type="reset">Cancel</button>
                    </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
            </form>

        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2017. Bracket. All Rights Reserved.</div>
            <div>Attentively and carefully made by ThemePixels.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5"
               href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5"
               href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-twitter tx-20"></i></a>
        </div>
    </footer>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<div id="modaldemo" class="modal fade effect-scale">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bd-0 rounded-0">
            <div class="modal-body pd-0">
                <div class="row flex-row-reverse no-gutters">
                    <div class="col-lg-6">
                        <div class="pd-30">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="pd-xs-x-30 pd-y-10">
                                <h5 class="tx-xs-28 tx-inverse mg-b-5">Welcome back!</h5>
                                <p>Sign in to your account to continue</p>
                                <br>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control pd-y-12"
                                           placeholder="Enter your email">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-20">
                                    <input type="email" name="password" class="form-control pd-y-12"
                                           placeholder="Enter your password">
                                    <a href="" class="tx-12 d-block mg-t-10">Forgot password?</a>
                                </div><!-- form-group -->

                                <button class="btn btn-primary pd-y-12 btn-block">Sign In</button>

                                <div class="mg-t-30 mg-b-20 tx-12">Don't have an account yet? <a href="">Sign Up</a>
                                </div>
                            </div>
                        </div><!-- pd-20 -->
                    </div><!-- col-6 -->
                    <div class="col-lg-6 bg-primary">
                        <div class="pd-40">
                            <h4 class="tx-white mg-b-20"><span>[</span> bracket + <span>]</span></h4>
                            <p class="tx-white op-7 mg-b-60">It is a long established fact that a reader will be
                                distracted by the readable content of a page when looking at its layout.</p>
                            <p class="tx-white tx-13">
                                <span class="tx-uppercase tx-medium d-block mg-b-15">Our Address:</span>
                                <span class="op-7">Ayala Center, Cebu Business Park, Cebu City, Cebu, Philippines 6000</span>
                            </p>
                        </div>
                    </div><!-- col-6 -->
                </div><!-- row -->
            </div><!-- modal-body -->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script src="<?= base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?= base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url() ?>public/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/moment/moment.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?= base_url() ?>public/admin/lib/peity/jquery.peity.js"></script>
<script src="<?= base_url() ?>public/admin/lib/highlightjs/highlight.pack.js"></script>
<script src="<?= base_url() ?>public/admin/lib/select2/js/select2.min.js"></script>

<script src="<?= base_url() ?>public/admin/js/bracket.js"></script>
<script>
    $(function () {
        'use strict'

        $('.form-layout .form-control').on('focusin', function () {
            $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.form-layout .form-control').on('focusout', function () {
            $(this).closest('.form-group').removeClass('form-group-active');
        });

        // Select2
        $('#select2-a, #select2-b').select2({
            minimumResultsForSearch: Infinity
        });

        $('#select2-a').on('select2:opening', function (e) {
            $(this).closest('.form-group').addClass('form-group-active');
        });

        $('#select2-a').on('select2:closing', function (e) {
            $(this).closest('.form-group').removeClass('form-group-active');
        });

    });
</script>
</body>
</html>
