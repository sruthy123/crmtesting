<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Modal Templates and Animation Design - Bracket Responsive Bootstrap 4 Admin Template</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/highlightjs/github.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>public/admin/css/bracket.css">
    <!--toastr--->
    <link href="<?php echo base_url() ?>public/toastr-master/build/toastr.css" rel="stylesheet" type="text/css"/>
    <style>
        a.menu_links { cursor: pointer; }

    </style>
</head>

<body>

<?php $this->load->view('admin/menu'); ?>

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="br-pageheader pd-y-15 pd-l-20">

    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
    </div>

    <div class="br-pagebody">
        <form method="post" action="<?=base_url()?>admins/sales/quotes/adding_quote">
            <div class="br-section-wrapper">
                <div class="row">
                    <div class="col-lg">
                        <label class="col-sm-8 form-control-label">
                            Quote Owner:<span class="tx-danger">*</span></label>
                        <input class="form-control" placeholder="Quote Owner" type="text" value="admin" disabled>
                    </div>
                    <div class="col-lg">
                        <label class="col-sm-8 form-control-label">

                            Quote name:<span class="tx-danger">*</span></label>
                        <input class="form-control" placeholder="Quote name" type="text" name="name">
                    </div><!-- col -->

                    <div class="col-lg mg-t-10 mg-lg-t-0">
                        <label class="col-sm-8 form-control-label">

                            Account:<span class="tx-danger">*</span></label>

                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Account"
                                tabindex="-1" aria-hidden="true" id="account" name="account_id">
                            <option label="Choose Account"></option>
                            <?php foreach ($accounts as $account) { ?>
                                <option value="<?= $account->encrypt_id; ?>"><?= $account->company; ?></option>
                            <?php } ?>
                        </select>
                    </div><!-- col -->
                    <div class="col-lg mg-t-10 mg-lg-t-0">
                        <label class="col-sm-8 form-control-label">

                            Deal:<span class="tx-danger">*</span></label>
                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Deal"
                                tabindex="-1" aria-hidden="true" name="deal_id">
                            <option value="">Choose</option>
                            <?php foreach ($deals as $deal) { ?>
                                <option value="<?= $deal->encrypt_id; ?>"><?= $deal->deal_name; ?></option>
                            <?php } ?>
                        </select>
                    </div><!-- col -->

                </div>

                <div class="pd-y-30 tx-left ">
                    <a href="" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
                       data-toggle="modal" data-target="#modaldemo1">Add Product/ services</a>
                </div><!-- pd-y-30 -->


                <div class="bd bd-gray-300 rounded table-responsive">
                    <table class="table mg-b-0" id="items">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Product/Service</th>
                            <th>Quantity</th>
                            <th>Unit Of Work</th>
                            <th>Unit Time</th>
                            <th>Unit Price</th>
                            <th>Line Price</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <!--                    <tr>-->
                        <!--                        <th scope="row">1</th>-->
                        <!--                        <td>-->
                        <!--                            PCI consult-->
                        <!--                            <textarea rows="1" class="form-control"-->
                        <!--                                      placeholder="Description" name="product_description[]">Description</textarea>-->
                        <!--                            <input type="text" name="product_id">-->
                        <!--                        </td>-->
                        <!--                        <td>-->
                        <!--                            2-->
                        <!--                            <input type="text" name="qty">-->
                        <!--                        </td>-->
                        <!--                        <td>5<input type="text" name="unit_of_work"></td>-->
                        <!--                        <td>day<input type="text" name="unit_time"></td>-->
                        <!--                        <td>3<input type="text" name="unit_price"></td>-->
                        <!--                        <td>30<input type="text" name="line_price"></td>-->
                        <!--                    </tr>-->
                        <tr>
                            <th scope="row"></th>
                            <td>

                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total</td>
                            <td><span id="total_amount">0</span></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="row mg-t-80">
                    <div class="col-xl-6">
                        <div class="form-layout form-layout-4">

                            <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Billing Address</h6>
                            <div class="row">
                                <label class="col-sm-4 form-control-label">
                                    Street:<span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" placeholder="Enter Street" name="billing_street"
                                           id="billing_street" disabled>
                                </div>
                            </div><!-- row -->
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label">
                                    City:<span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" placeholder="Enter City" name="billing_city"
                                           id="billing_city" disabled>
                                </div>
                            </div>
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label"> State:<span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" placeholder="Enter State" name="billing_state"
                                           id="billing_state" disabled>
                                </div>
                            </div>
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label"> Zip Code:<span
                                            class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" placeholder="Enter Zip Code"
                                           name="billing_zip_code" id="billing_zip_code" disabled>
                                </div>
                            </div><!-- row -->
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label"> Country:<span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select class="form-control select2" data-placeholder="Choose country"
                                            name="billing_country" id="billing_country" disabled>
                                        <option label="Choose country"></option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?= $country->country_id; ?>"><?= $country->long_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- form-layout -->
                    </div><!-- col-6 -->
                    <div class="col-xl-6 mg-t-20 mg-xl-t-0">
                        <div class="form-layout form-layout-5">
                            <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Shipping Address</h6>
                            <div class="row">
                                <label class="col-sm-4 form-control-label">
                                    Street:</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" placeholder="Enter Street" id="shipping_street"
                                           name="shipping_street" disabled>
                                </div>
                            </div><!-- row -->
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label">
                                    City:</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" placeholder="Enter City" id="shipping_city"
                                           name="shipping_city" disabled>
                                </div>
                            </div>
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label"> State:</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" placeholder="Enter State" id="shipping_state"
                                           name="shipping_state" disabled>
                                </div>
                            </div>
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label"> Zip Code:</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <input type="text" class="form-control" placeholder="Enter Zip Code"
                                           id="shipping_zip_code" name="shipping_zip_code" disabled>
                                </div>
                            </div><!-- row -->
                            <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label"> Country:</label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                    <select class="form-control select2" data-placeholder="Choose country"
                                            name="shipping_country" id="shipping_country" disabled>
                                        <option label="Choose country"></option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?= $country->country_id; ?>"><?= $country->long_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- form-layout -->
                    </div><!-- col-6 -->
                </div>

                <div class="col-xl-12">
                    <div class="row mg-t-20">
                        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Terms and Conditions</h6>
                        <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                            <textarea rows="5" class="form-control"
                                      placeholder="Enter your address" name="terms_conditions"><?= $terms_conditions[0]->terms_conditions; ?></textarea>
                        </div>
                    </div>

                    <div class="form-layout-footer mg-t-30">
                        <button class="btn btn-info" type="submit" name="submit">Submit</button>
                        <button class="btn btn-secondary" type="reset">Cancel</button>
                    </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
            </div>
        </form>
    </div><!-- br-section-wrapper -->

</div><!-- br-pagebody -->
<footer class="br-footer">
    <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; 2017. Bracket. All Rights Reserved.</div>
        <div>Attentively and carefully made by ThemePixels.</div>
    </div>
    <div class="footer-right d-flex align-items-center">
        <span class="tx-uppercase mg-r-10">Share:</span>
        <a target="_blank" class="pd-x-5"
           href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i
                    class="fa fa-facebook tx-20"></i></a>
        <a target="_blank" class="pd-x-5"
           href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i
                    class="fa fa-twitter tx-20"></i></a>
    </div>
</footer>
<!-- ########## END: MAIN PANEL ########## -->

<!-- BASIC MODAL -->
<div id="modaldemo1" class="modal fade">
    <div class="modal-dialog modal-dialog-vertical-center" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add Product /Service</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-25">


                <div class="row">
                    <label class="col-sm-6 form-control-label">Product/Service: <span class="tx-danger">*</span></label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <select class="form-control" id="pro">
                            <option label="Choose"></option>
                            <?php foreach ($product_services as $product_service) { ?>
                                <option value="<?= $product_service->encrypt_id; ?>"><?= $product_service->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div><!-- row -->
                <div class="row mg-t-20">
                    <label class="col-sm-7 form-control-label">Quatity: <span class="tx-danger">*</span></label>
                    <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                        <input type="number" class="form-control" id="pro_qty">
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-7 form-control-label">Unit of work: </label>
                    <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                        <input type="number" class="form-control" id="unit_of_work">
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label">Unit Time: </label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <select class="form-control" id="unit_type">
                            <option label="Choose Unit Type"></option>
                            <option value="Day">Day</option>
                            <option value="Hr">Hr</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-7 form-control-label">Unit Price: <span class="tx-danger">*</span></label>
                    <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                        <input type="text" class="form-control" placeholder="Unit Price" id="unit_price">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
                        data-dismiss="modal" id="add">Add
                </button>
                <button type="button" class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->


<script src="<?= base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?= base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url() ?>public/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/moment/moment.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?= base_url() ?>public/admin/lib/peity/jquery.peity.js"></script>
<script src="<?= base_url() ?>public/admin/lib/highlightjs/highlight.pack.js"></script>

<script src="<?= base_url() ?>public/admin/js/bracket.js"></script>
<script>
    $(function () {

        // showing modal with effect
        $('.modal-effect').on('click', function () {
            var effect = $(this).attr('data-effect');
            $('#modaldemo8').addClass(effect, function () {
                $('#modaldemo8').modal('show');
            });
            return false;
        });

        // hide modal with effect
        $('#modaldemo8').on('hidden.bs.modal', function (e) {
            $(this).removeClass(function (index, className) {
                return (className.match(/(^|\s)effect-\S+/g) || []).join(' ');
            });
        });
    });
</script>
<!-----------sruthy---------------->
<script>
    $("#add").click(function () {
        //calculate all values of the row
        var row_count = ($("#items tr").length) - 1;
        var product_name = $("#pro option:selected").text();
        var product_id = $("#pro option:selected").val();
        var pro_qty = $("#pro_qty").val();
        var unit_of_work = $("#unit_of_work").val();
        var unit_type = $("#unit_type option:selected").val();
        var unit_price = $("#unit_price").val();
        //calculating the unit price
        if (unit_of_work == "") {
            var line_price = pro_qty * unit_price;
        } else {
            var line_price = pro_qty * unit_price * unit_of_work;
        }

        // upending row to the table
        $('#items tr:last').before("<tr class='products'>" +
            "<th scope='row'>" + row_count + "</th>" +
            "<td>" + product_name + "<textarea rows='1' class='form-control' placeholder='Description' name='product_description[]'>Description</textarea><input type='hidden' name='item_id[]' value='" + product_id + "'></td>" +
            "<td>" + pro_qty + "<input type='hidden' name='qty[]' value='" + pro_qty + "'></td>" +
            "<td>" + unit_of_work + "<input type='hidden' name='unit_of_work[]' value='" + unit_of_work + "'></td>" +
            "<td>" + unit_type + "<input type='hidden' name='unit_time[]' value='" + unit_type + "'></td>" +
            "<td>" + unit_price + "<input type='hidden' name='unit_price[]' value='" + unit_price + "'></td>" +
            "<td class='line_price'>" + line_price + "</td>" +
            "<td><a class='remove' style='cursor: pointer;'>X</a></td>" +
            "</tr>");
        //update the total using function
        update_amounts(line_price);


    });

    function update_amounts(line_price) {
        //total_amount is set to 0(text) initially. then add the line price to it. + is used to convert text to integer
        var total_amount = +($("#total_amount").text());
        $("#total_amount").text(total_amount + line_price);
    }

</script>
<!-------fetching shipping and billing address according to account------------>

<script>
    $("#account").on('change', function () {
        var account_id = $(this).val();
        $.ajax({
            type: "POST",
            dataType: "json",
            data: {account_id: account_id},
            url: "<?php echo base_url() ?>admins/sales/quotes/get_account_details",
            success: function (data) {
                $("#shipping_street").val(data["shipping_address"][0]["shipping_street"]);
                $("#shipping_city").val(data["shipping_address"][0]["shipping_city"]);
                $("#shipping_state").val(data["shipping_address"][0]["shipping_state"]);
                $("#shipping_zip_code").val(data["shipping_address"][0]["shipping_zip_code"]);
                $("#shipping_country").val(data["shipping_address"][0]["shipping_country"]);
                // console.log(data["shipping_address"][0]["shipping_city"]);
                $("#billing_street").val(data["billing_address"][0]["billing_street"]);
                $("#billing_city").val(data["billing_address"][0]["billing_city"]);
                $("#billing_state").val(data["billing_address"][0]["billing_state"]);
                $("#billing_zip_code").val(data["billing_address"][0]["billing_zip_code"]);
                $("#billing_country").val(data["billing_address"][0]["billing_country"]);
                // console.log(data["billing_address"][0]["billing_country"]);

            }
        });
    });
</script>
<!-----------sruthy---------------->
<!---toastr script--->
<script src="<?php echo base_url() ?>public/toastr-master/toastr.js"></script>
<script>
    var error_msg = "<?php echo $this->session->flashdata('error_msg');?>";
    if (error_msg != "") {
        toastr.error(error_msg, {positionClass: 'toast-top-right', timeOut: -1})
    }
    var success_msg = "<?php echo $this->session->flashdata('success_msg');?>";
    if (success_msg != "") {
        toastr.success(success_msg, {positionClass: 'toast-top-right', timeOut: -1})
    }
</script>
<script>

    $('body').on('click', 'a.remove', function() {
        var line_price;
        line_price= $(this).closest('tr').children('td.line_price').text();
        update_amounts(-line_price);
        $(this).closest('tr').remove();
    });
</script>
</body>
</html>
