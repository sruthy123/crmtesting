<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracket/img/bracket-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracket/img/bracket-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Typography Design - Bracket Responsive Bootstrap 4 Admin Template</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>public/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/admin/lib/highlightjs/github.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>public/admin/css/bracket.css">
</head>

<body>

<?php $this->load->view('admin/menu'); ?>

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">

    </div>

    <div class="br-pagebody">
        <div class="br-section-wrapper">


            <div class="pd-30 bd">
                <dl class="row">
                    <dt class="col-sm-12 tx-inverse">QUOTE DETAILS</dt>
                    <dt class="col-sm-3 tx-inverse">Quote Name</dt>
                    <dd class="col-sm-9"><?= $quote_basic_details[0]->name; ?></dd>

                    <dt class="col-sm-3 tx-inverse">Quote Owner</dt>
                    <dd class="col-sm-9">admin</dd>

                    <dt class="col-sm-3 tx-inverse">Account</dt>
                    <dd class="col-sm-9"><?= $quote_basic_details[0]->company; ?></dd>

                    <dt class="col-sm-3 tx-inverse">Zip code</dt>
                    <dd class="col-sm-9"><?= $quote_basic_details[0]->deal_name; ?></dd>

                </dl>
                <dl class="row">
<!--                    <dt class="col-sm-2 tx-inverse">Quote Name</dt>-->
                    <dd class="col-sm-12">
                    <div class="bd bd-gray-300 rounded table-responsive">
                        <table class="table mg-b-0" id="items">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product/Service</th>
                                <th>Quantity</th>
                                <th>Unit Of Work</th>
                                <th>Unit Time</th>
                                <th>Unit Price</th>
                                <th>Line Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($quote_items as $key=>$quote_item){?>
                                <tr>
                                    <th scope="row"><?=$key+1;?></th>
                                    <td>
                                        <?=$quote_item->name;?>
                                        <br/>
                                        <br/>
                                        <?=$quote_item->product_description;?>
                                    </td>
                                    <td><?=$quote_item->qty;?></td>
                                    <td><?=($quote_item->unit_of_work==0)?"":$quote_item->unit_of_work;?></td>
                                    <td><?=$quote_item->unit_time;?></td>
                                    <td><?=$quote_item->unit_price;?></td>
                                    <td><?=$quote_item->line_total;?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <th scope="row"></th>
                                <td>

                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total</td>
                                <td><span id="total_amount"><?=$total_amount?></span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    </dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-12 tx-inverse">BILLING ADDRESS</dt>
                    <dt class="col-sm-3 tx-inverse">street</dt>
                    <dd class="col-sm-9"><?= $billing_address[0]->billing_street; ?></dd>

                    <dt class="col-sm-3 tx-inverse">City</dt>
                    <dd class="col-sm-9"><?= $billing_address[0]->billing_city; ?></dd>

                    <dt class="col-sm-3 tx-inverse">State</dt>
                    <dd class="col-sm-9"><?= $billing_address[0]->billing_state; ?></dd>

                    <dt class="col-sm-3 tx-inverse">Zip code</dt>
                    <dd class="col-sm-9"><?= $billing_address[0]->billing_zip_code; ?></dd>

                    <dt class="col-sm-3 tx-inverse">Country</dt>
                    <dd class="col-sm-9"><?= $billing_address[0]->long_name; ?></dd>


                </dl>

                <dl class="row">
                    <?php if (!empty($shipping_address)) { ?>
                        <dt class="col-sm-12 tx-inverse">SHIPPING ADDRESS</dt>
                        <dt class="col-sm-3 tx-inverse">street</dt>
                        <dd class="col-sm-9"><?= $shipping_address[0]->shipping_street; ?></dd>

                        <dt class="col-sm-3 tx-inverse">City</dt>
                        <dd class="col-sm-9"><?= $shipping_address[0]->shipping_city; ?></dd>

                        <dt class="col-sm-3 tx-inverse">State</dt>
                        <dd class="col-sm-9"><?= $shipping_address[0]->shipping_state; ?></dd>

                        <dt class="col-sm-3 tx-inverse">Zip code</dt>
                        <dd class="col-sm-9"><?= $shipping_address[0]->shipping_zip_code; ?></dd>

                        <dt class="col-sm-3 tx-inverse">Country</dt>
                        <dd class="col-sm-9"><?= $shipping_address[0]->long_name; ?></dd>
                    <?php } ?>



                </dl>
                <dl class="row">
                    <dt class="col-sm-12 tx-inverse">TERMS AND CONDITIONS</dt>
                    <dt class="col-sm-1 tx-inverse"></dt>
                    <dd class="col-sm-10"><?= $terms_conditions[0]->terms_conditions; ?></dd>

                    <dd class="col-sm-9">
                        <dl class="row">
                            <dd class="col-sm-8">
                                <!--                                <a href="-->
                                <? //=base_url()?><!--admins/sales/customers/edit_account_details" class="btn btn-success btn-with-icon">-->
                                <!--                                    <div class="ht-40 justify-content-between">-->
                                <!--                                        <span class="icon wd-40"><i class="fa fa-edit"></i></span>-->
                                <!--                                    </div>-->
                                <!--                                </a>-->
                                <a class="btn btn-danger btn-with-icon remove"
                                   data-encrypt_id="<?= $quote_basic_details[0]->encrypt_id; ?>">
                                    <div class="ht-40 justify-content-between">
                                        <span class="icon wd-40"><i class="fa fa-trash"></i></span>
                                    </div>
                                </a>
                            </dd>

                        </dl>
                    </dd>


                </dl>
            </div>

        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2017. Bracket. All Rights Reserved.</div>
            <div>Attentively and carefully made by ThemePixels.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5"
               href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5"
               href="https://twitter.com/home?status=Bracket,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracket/intro"><i
                        class="fa fa-twitter tx-20"></i></a>
        </div>
    </footer>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="<?= base_url() ?>public/admin/lib/jquery/jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?= base_url() ?>public/admin/lib/popper.js/popper.js"></script>
<script src="<?= base_url() ?>public/admin/lib/bootstrap/bootstrap.js"></script>
<script src="<?= base_url() ?>public/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="<?= base_url() ?>public/admin/lib/moment/moment.js"></script>
<script src="<?= base_url() ?>public/admin/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="<?= base_url() ?>public/admin/lib/peity/jquery.peity.js"></script>
<script src="<?= base_url() ?>public/admin/lib/highlightjs/highlight.pack.js"></script>

<script src="<?= base_url() ?>public/admin/js/bracket.js"></script>
<!---sruthy--->
<!---removed because no such a file create 404 in js--->
<!--<script src="--><? //= base_url() ?><!--public/admin/js/tooltip-colored.js"></script>-->
<!--<script src="--><? //= base_url() ?><!--public/admin/js/popover-colored.js"></script>-->
<script>
    $(".remove").click(function () {
        if (confirm('Are you sure to remove this item ?')) {
            var encrypt_id = $(this).data("encrypt_id");
            window.location.href = "<?= base_url() ?>admins/sales/quotes/delete_quote_details/" + encrypt_id;
        }
    });
</script>
</body>
</html>
