<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    /**************Go to dashboard************/
    public function index()
    {
        $this->load->view('admin/dashboard');
    }
    /**************Go to dashboard************/
    public function test()
    {
        $this->load->view('admin/test');
    }
}
